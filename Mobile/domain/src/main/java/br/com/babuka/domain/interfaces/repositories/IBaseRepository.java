package br.com.babuka.domain.interfaces.repositories;

import br.com.babuka.domain.interfaces.entitites.IEntity;
import br.com.babuka.domain.interfaces.entitites.IEntityFactory;

public interface IBaseRepository<T extends IEntity, F extends IEntityFactory> {

    void Delete(String documentId) ;

    String Insert(T entity) ;

    void Update(T entity) ;

    T GetById(String entity);

    //String CreateAttachment(String entityName);

    //T GetByIdAndType(String entity);

    //List<HashMap<String, T>> GetConflicts(String documentId);

    //void SolveConflicts(String documentId, String winnerRevisionId);
}
