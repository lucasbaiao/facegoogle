package br.com.babuka.domain.entities;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import br.com.babuka.domain.interfaces.components.FilterableModel;
import br.com.babuka.domain.interfaces.entitites.IEntity;
import br.com.babuka.domain.interfaces.entitites.IEntityFactory;
import br.com.babuka.domain.interfaces.entitites.IFactory;

public class Category implements IEntity<Category, Category.CategoryFactory> , FilterableModel{

    public static CategoryFactory Factory = new CategoryFactory();
    private String title;
    private int level;
    private String group;
    private Boolean isLastLevel;
    private String documentId;

    public String getTitle() {
        return title;
    }

    public int getLevel() {
        return level;
    }

    public String getGroup() {
        return group;
    }

    public Boolean getLastLevel() {
        return isLastLevel;
    }

    private Category(String title, int level, String group, Boolean isLastLevel) {
        this.title = title;
        this.level = level;
        this.group = group;
        this.isLastLevel = isLastLevel;
    }

    @NotNull
    @Override
    public String[] getFilterProperties() {
        return new String[]{title};
    }

    @NotNull
    @Override
    public IFactory<Category, CategoryFactory> getFactoryInstance() {
        return new CategoryFactory();
    }

    @Nullable
    @Override
    public String getDocumentId() {
        return documentId;
    }

    public static class CategoryFactory implements IEntityFactory<Category, CategoryFactory> {
        private Category instance;

        @Override
        public CategoryFactory create(Map<String, Object> jsonObject) {
            return this;
        }

        @Override
        public Category getInstance() {
            return instance;
        }

        @Override
        public CategoryFactory from(Category obj) {
            return this;
        }

        @Override
        public CategoryFactory withId(String id) {
            return this;
        }

        @NotNull
        public CategoryFactory fromParent(@Nullable Category parent) {
            instance.group = parent.documentId;
            return this;
        }
    }
}
