package br.com.babuka.domain.entities;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import br.com.babuka.domain.interfaces.entitites.IEntity;
import br.com.babuka.domain.interfaces.entitites.IEntityFactory;
import br.com.babuka.domain.interfaces.entitites.IFactory;
import br.com.babuka.domain.values.Endereco;

public class Empresa implements IEntity<Empresa, Empresa.EmpresaFactory> {

    private String documentId;
    private String razaoSocial;
    private String nomeFantasia;
    private String nomeResponsavel;
    private String cnpj;
    private String email;
    private int codigo;
    private Endereco endereco;

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getEmail() {
        return email;
    }

    public int getCodigo() {
        return codigo;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    private Empresa(String razaoSocial, String nomeFantasia, String nomeResponsavel, String cnpj,
                    String email, int codigo, Endereco endereco) {
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.nomeResponsavel = nomeResponsavel;
        this.cnpj = cnpj;
        this.email = email;
        this.codigo = codigo;
        this.endereco = endereco;
    }

    @Nullable
    @Override
    public String getDocumentId() {
        return null;
    }

    @NotNull
    @Override
    public IFactory<Empresa, EmpresaFactory> getFactoryInstance() {
        return new EmpresaFactory();
    }

    public static class EmpresaFactory implements IEntityFactory<Empresa, Empresa.EmpresaFactory> {
        private Empresa instance;

        @Override
        public EmpresaFactory create(Map<String, Object> jsonObject) {
            instance = new Empresa(
                    (String) jsonObject.get("RazaoSocial"),
                    (String) jsonObject.get("NomeFantasia"),
                    (String) jsonObject.get("NomeResponsavel"),
                    (String) jsonObject.get("Cnpj"),
                    (String) jsonObject.get("Email"),
                    (int) jsonObject.get("Codigo"),
                    Endereco.Factory.create((Map<String, Object>) jsonObject.get("Endereco")).getInstance()
            );
            return this;
        }

        @Override
        public EmpresaFactory from(Empresa obj) {
            instance = new Empresa(obj.razaoSocial, obj.nomeFantasia, obj.nomeResponsavel,
                    obj.cnpj, obj.email, obj.codigo, obj.endereco);
            return this;
        }

        @Override
        public EmpresaFactory withId(String id) {
            instance.documentId = id;
            return this;
        }

        @Override
        public Empresa getInstance() {
            return instance;
        }
    }
}
