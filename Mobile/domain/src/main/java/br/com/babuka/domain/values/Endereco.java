package br.com.babuka.domain.values;

import java.util.Map;

import br.com.babuka.domain.interfaces.entitites.IFactory;

public class Endereco {

    public static EnderecoFactory Factory = new EnderecoFactory();

    private Logradouro logradouro;
    private Municipio municipio;
    private String complemento;

    public Logradouro getLogradouro() {
        return logradouro;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public String getComplemento() {
        return complemento;
    }

    private Endereco(Logradouro logradouro, Municipio municipio, String complemento) {
        this.logradouro = logradouro;
        this.municipio = municipio;
        this.complemento = complemento;
    }

    public static class EnderecoFactory implements IFactory<Endereco, EnderecoFactory> {

        private Endereco instance;

        EnderecoFactory create(Logradouro logradouro, Municipio municipio, String complemento) {
            instance = new Endereco(logradouro, municipio, complemento);
            return this;
        }

        @Override
        public EnderecoFactory create(Map<String, Object> jsonObject) {
            return create(
                    Logradouro.Factory.create((Map<String, Object>) jsonObject.get("Logradouro")).getInstance(),
                    Municipio.Factory.create((Map<String, Object>) jsonObject.get("Municipio")).getInstance(),
                    (String)jsonObject.get("Complemento")
            );
        }

        @Override
        public Endereco getInstance() {
            return instance;
        }
    }
}
