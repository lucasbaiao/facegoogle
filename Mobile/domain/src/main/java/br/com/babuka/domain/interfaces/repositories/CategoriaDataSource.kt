package br.com.babuka.domain.interfaces.repositories

import br.com.babuka.domain.entities.Category

interface CategoriaDataSource : IBaseRepository<Category, Category.CategoryFactory> {

    fun getAll(parentId: String): List<Category>

    fun getAll(): List<Category>
}
