package br.com.babuka.domain.values.enums

class TipoPostEnum protected constructor( val codigo: Int,  val valor: String) {

    companion object {
        var Cardapio = TipoPostEnum(1, "Cardapio")
        var Promocao = TipoPostEnum(2, "Promocao")
    }
}
