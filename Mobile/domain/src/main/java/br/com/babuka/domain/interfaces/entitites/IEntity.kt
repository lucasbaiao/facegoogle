package br.com.babuka.domain.interfaces.entitites

interface IEntity<T : IEntity<T, K>, K : IFactory<T, K>?> {

    val factoryInstance: IFactory<T, K>

    val documentId: String?
}