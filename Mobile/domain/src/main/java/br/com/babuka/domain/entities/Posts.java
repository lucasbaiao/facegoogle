package br.com.babuka.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

import br.com.babuka.domain.interfaces.entitites.IEntity;
import br.com.babuka.domain.interfaces.entitites.IEntityFactory;
import br.com.babuka.domain.interfaces.entitites.IFactory;
import br.com.babuka.domain.interfaces.values.ITipoPost;
import br.com.babuka.domain.values.PostTipoCardapio;
import br.com.babuka.domain.values.PostTipoPromocao;
import br.com.babuka.domain.values.enums.TipoPostEnum;

public class Posts implements IEntity<Posts, Posts.PostFactory> {

    public static Posts.PostFactory Factory = new PostFactory();

    private String documentId;
    private ITipoPost tipo;
    private String title;
    private String description;
    private ArrayList<String> keywords;
    private int views = 0;
    private InputStream image;

   public InputStream getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getViews() {
        return views;
    }

    public ArrayList<String> getKeywords() {
        return keywords;
    }

    private Posts() {
        super();
    }

    private Posts(String title, String description) {
        this.title = title;
        this.description = description;
    }

    @Override
    public int hashCode() {
        int result = 37;
        result = 31 * result + documentId.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + description.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Posts)) {
            return false;
        }

        Posts lhs = (Posts) o; // lhs means "left hand side"

        return documentId.equals(lhs.documentId);
    }

    @NotNull
    @Override
    public IFactory<Posts, PostFactory> getFactoryInstance() {
        return new PostFactory();
    }

    @Nullable
    @Override
    public String getDocumentId() {
        return documentId;
    }

    public static class PostFactory implements IEntityFactory<Posts, PostFactory> {

        @JsonIgnore
        private Posts instance;

        public PostFactory() {
            super();
        }

        @Override
        public Posts getInstance() {
            return instance;
        }

        @Override
        public PostFactory create(Map<String, Object> jsonObject) {
            Map<String, Object> tipo = (Map<String, Object>) jsonObject.get("Tipo");
            Integer codigo = (Integer) tipo.get("Codigo");

            PostTipoPromocao.PostTipoPromocaoFactory tipoFactory;
            if(TipoPostEnum.Companion.getCardapio().getCodigo() == codigo) {

            } else {
                tipoFactory = PostTipoPromocao.factory.create(tipo);
            }

            return create((String) jsonObject.get("Title"), (String) jsonObject.get("Description"))
                    .withKeywords((ArrayList<String>) jsonObject.get("Keywords"));
        }

        private PostFactory create(String title, String description) {
            instance = new Posts(title, description);
            return this;
        }

        @Override
        public PostFactory from(Posts obj) {
            instance = new Posts(obj.title, obj.description);
            instance.documentId = obj.documentId;
            instance.keywords = obj.keywords;
            return this;
        }

        public PostFactory withKeywords(ArrayList<String> keys) {
            instance.keywords = keys;
            return this;
        }

        public PostFactory withImagem(InputStream stream) {
            instance.image = stream;
            return this;
        }

        @Override
        public PostFactory withId(String id) {
            instance.documentId = id;
            return this;
        }
    }
}