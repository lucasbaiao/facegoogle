package br.com.babuka.domain.values;

import java.util.Map;

import br.com.babuka.domain.interfaces.entitites.IFactory;

public class Municipio {

    public static MunicipioFactory Factory = new MunicipioFactory();

    private int codigo;
    private String nome;

    public int getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }

    private Municipio(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public static class MunicipioFactory implements IFactory<Municipio, Municipio.MunicipioFactory> {

        private Municipio instance;

        MunicipioFactory Create(int codigo, String nome) {
            instance = new Municipio(codigo, nome);
            return this;
        }

        @Override
        public MunicipioFactory create(Map<String, Object> jsonObject) {
            return Create((int) jsonObject.get("Codigo"), (String) jsonObject.get("Nome"));
        }

        @Override
        public Municipio getInstance() {
            return instance;
        }
    }
}
