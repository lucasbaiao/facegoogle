package br.com.babuka.domain.interfaces.entitites;

import java.util.Map;

public interface IFactory<T, K extends IFactory<T, K>>  {

    K create(Map<String, Object> jsonObject);

    T getInstance();
}
