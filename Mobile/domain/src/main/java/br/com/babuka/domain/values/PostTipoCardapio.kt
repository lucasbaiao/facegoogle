package br.com.babuka.domain.values

import br.com.babuka.domain.interfaces.values.ITipoPost
import br.com.babuka.domain.values.enums.TipoPostEnum

class PostTipoCardapio : ITipoPost {

    override var codigo: Int = TipoPostEnum.Cardapio.codigo


}