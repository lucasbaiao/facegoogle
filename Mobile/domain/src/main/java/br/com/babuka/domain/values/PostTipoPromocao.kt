package br.com.babuka.domain.values

import br.com.babuka.domain.interfaces.entitites.IFactory
import br.com.babuka.domain.interfaces.values.ITipoPost
import br.com.babuka.domain.values.enums.TipoPostEnum
import com.fasterxml.jackson.annotation.JsonIgnore

class PostTipoPromocao : ITipoPost {

    companion object {
        @JvmStatic
        var factory: PostTipoPromocao.PostTipoPromocaoFactory = PostTipoPromocaoFactory()
    }


    override var codigo: Int = TipoPostEnum.Promocao.codigo
    var preco: Double? = null
    var percentOff: Int? = null

    private constructor()

    private constructor(preco: Double, percentOff: Int) {
        this.preco = preco
        this.percentOff = percentOff
    }

    class PostTipoPromocaoFactory : IFactory<PostTipoPromocao, PostTipoPromocaoFactory> {

        @JsonIgnore
        private var instance: PostTipoPromocao = PostTipoPromocao()

        fun create(preco: Double, percentOff: Int): PostTipoPromocaoFactory {
            instance = PostTipoPromocao(preco, percentOff)
            return this
        }

        override fun create(jsonObject: MutableMap<String, Any>?): PostTipoPromocaoFactory {
            return create(jsonObject?.get("Preco") as Double, jsonObject["PercentOff"] as Int)
        }

        override fun getInstance(): PostTipoPromocao {
            return instance
        }
    }
}