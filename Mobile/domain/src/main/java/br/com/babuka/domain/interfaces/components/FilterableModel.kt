package br.com.babuka.domain.interfaces.components

interface FilterableModel {

    val filterProperties : Array<String>
}