package br.com.babuka.domain.values;


import java.util.Map;

import br.com.babuka.domain.interfaces.entitites.IFactory;

public class Logradouro {

    public static LogradouroFactory Factory = new LogradouroFactory();

    private String rua;
    private String numero;
    private String bairro;
    private String cep;

    public String getRua() {
        return rua;
    }

    public String getNumero() {
        return numero;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCep() {
        return cep;
    }

    private Logradouro(String rua, String numero, String bairro, String cep) {
        this.rua = rua;
        this.numero = numero;
        this.bairro = bairro;
        this.cep = cep;
    }

    public static class LogradouroFactory implements IFactory<Logradouro, Logradouro.LogradouroFactory> {

        private Logradouro instance;

        LogradouroFactory Create(String rua, String numero, String bairro, String cep) {
            instance = new Logradouro(rua, numero, bairro, cep);
            return this;
        }

        @Override
        public Logradouro.LogradouroFactory create(Map<String, Object> jsonObject) {
            return Create((String) jsonObject.get("Rua"), (String) jsonObject.get("Numero"),
                    (String) jsonObject.get("Bairro"), (String) jsonObject.get("Cep"));
        }

        @Override
        public Logradouro getInstance() {
            return instance;
        }
    }
}
