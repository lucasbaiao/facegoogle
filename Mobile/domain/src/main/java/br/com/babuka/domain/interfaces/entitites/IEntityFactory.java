package br.com.babuka.domain.interfaces.entitites;

public interface IEntityFactory<T extends IEntity<T, K>, K extends IEntityFactory<T, K>>
        extends IFactory<T, K> {

    K from(T obj);

    K withId(String id);
}