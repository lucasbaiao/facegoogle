package br.com.babuka.domain.interfaces.repositories

import br.com.babuka.domain.entities.Posts

interface PostsDataSource {

    fun getAll(): List<Posts>
}