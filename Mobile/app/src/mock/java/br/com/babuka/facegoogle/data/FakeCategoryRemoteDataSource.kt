package br.com.babuka.facegoogle.data

import br.com.babuka.domain.entities.Category
import br.com.babuka.domain.interfaces.repositories.CategoriaDataSource


/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
class FakeCategoryRemoteDataSource {

    private val TASKS_SERVICE_DATA = LinkedHashMap<String, Category>()

    /*
    constructor() {
        TASKS_SERVICE_DATA.put("1", Category.Factory.create("Acessórios para Veículos", 1).instance)
        TASKS_SERVICE_DATA.put("2", Category.Factory.create("Alimentos e Bebidas", 1).instance)
        TASKS_SERVICE_DATA.put("3", Category.Factory.create("Brinquedos e Hobbies", 1).instance)
        TASKS_SERVICE_DATA.put("4", Category.Factory.create("Eletrônicos, Áudio e Vídeo", 1).instance)
        TASKS_SERVICE_DATA.put("5", Category.Factory.create("Eletrodomésticos", 1).instance)
        TASKS_SERVICE_DATA.put("578", Category.Factory.create("Calçados, Roupas e Bolsas", 1).instance)

        //Acessórios para Veículos - 1
        getAcessoriosVeiculos()
        //Alimentos e Bebidas - 2
        getAlimentosBebidas()
        //Brinquedos e Hobbies - 3
        getBrinquedosHobbies()
        //Eletrônicos, Áudio e Vídeo - 4
        getEletrônicosÁudioVídeo()
        // Eletrodomésticos - 5
        getEletrodomésticos()
        // Eletrodomésticos - 578
        getCalcadosRoupasBolsas()
    }

    private fun getCalcadosRoupasBolsas() {
        TASKS_SERVICE_DATA.put("579", Category.Factory.create("Masculino", 2).instance)
        TASKS_SERVICE_DATA.put("580", Category.Factory.create("Feminino", 2).instance)
        TASKS_SERVICE_DATA.put("581", Category.Factory.create("Infantil", 2).instance)

        TASKS_SERVICE_DATA.put("582", Category.Factory.create("SAPATOS", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("583", Category.Factory.create("TÊNIS", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("584", Category.Factory.create("BOLSAS", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("585", Category.Factory.create("MODA PRAIA", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("586", Category.Factory.create("VESTIDOS", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("587", Category.Factory.create("CAMISETAS E BLUSAS", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("588", Category.Factory.create("Acessórios da Moda", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("589", Category.Factory.create("Bermudas", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("590", Category.Factory.create("Calças", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("591", Category.Factory.create("Camisas", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("592", Category.Factory.create("Casacos", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("593", Category.Factory.create("Macacão", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("594", Category.Factory.create("Mochilas", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("595", Category.Factory.create("Moda Íntima e Lingerie", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("596", Category.Factory.create("Saias", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("597", Category.Factory.create("Shorts", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("598", Category.Factory.create("Ternos", 3, TASKS_SERVICE_DATA.get("579")!!.documentId).instance)

        TASKS_SERVICE_DATA.put("599", Category.Factory.create("SAPATOS", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("600", Category.Factory.create("TÊNIS", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("601", Category.Factory.create("BOLSAS", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("602", Category.Factory.create("MODA PRAIA", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("603", Category.Factory.create("VESTIDOS", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("604", Category.Factory.create("CAMISETAS E BLUSAS", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("605", Category.Factory.create("Acessórios da Moda", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("606", Category.Factory.create("Bermudas", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("607", Category.Factory.create("Calças", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("608", Category.Factory.create("Camisas", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("609", Category.Factory.create("Casacos", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("610", Category.Factory.create("Macacão", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("611", Category.Factory.create("Mochilas", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("612", Category.Factory.create("Moda Íntima e Lingerie", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("613", Category.Factory.create("Saias", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("614", Category.Factory.create("Shorts", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("615", Category.Factory.create("Ternos", 3, TASKS_SERVICE_DATA.get("580")!!.documentId).instance)

        TASKS_SERVICE_DATA.put("616", Category.Factory.create("Bermudas", 3, TASKS_SERVICE_DATA.get("581")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("617", Category.Factory.create("Calças", 3, TASKS_SERVICE_DATA.get("581")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("618", Category.Factory.create("Camisas", 3, TASKS_SERVICE_DATA.get("581")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("619", Category.Factory.create("Casacos", 3, TASKS_SERVICE_DATA.get("581")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("620", Category.Factory.create("Macacão", 3, TASKS_SERVICE_DATA.get("581")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("621", Category.Factory.create("Shorts", 3, TASKS_SERVICE_DATA.get("581")!!.documentId).instance)
    }

    private fun getEletrodomésticos() {
        TASKS_SERVICE_DATA.put("534", Category.Factory.create("Ar e Ventilação", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("535", Category.Factory.create("Bebedouros e Purificadores", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("536", Category.Factory.create("Geladeiras e Freezers", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("537", Category.Factory.create("Forno e Fogões", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("538", Category.Factory.create("Eletroportáteis", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("539", Category.Factory.create("Lava Louças e Acessórios", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("540", Category.Factory.create("Máquinas de Lavar", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("541", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("5")!!.documentId).instance)


        /// Ar e Ventilação
        TASKS_SERVICE_DATA.put("542", Category.Factory.create("Aquecedores de Ar", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("543", Category.Factory.create("Ar Condicionado", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("544", Category.Factory.create("Circulador de Ar", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("545", Category.Factory.create("Climatizador", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("546", Category.Factory.create("Cortina de Ar", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("547", Category.Factory.create("Desumidificador", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("548", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("549", Category.Factory.create("Umidificador", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("550", Category.Factory.create("Ventilador", 3, TASKS_SERVICE_DATA.get("534")!!.documentId).instance)

        /// Bebedouros e Purificadores
        TASKS_SERVICE_DATA.put("551", Category.Factory.create("Bebedouro", 3, TASKS_SERVICE_DATA.get("535")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("552", Category.Factory.create("Filtro de Água", 3, TASKS_SERVICE_DATA.get("535")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("553", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("535")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("554", Category.Factory.create("Purificador", 3, TASKS_SERVICE_DATA.get("535")!!.documentId).instance)

        /// Geladeiras e Freezers
        TASKS_SERVICE_DATA.put("555", Category.Factory.create("Adegas Climatizadas", 3, TASKS_SERVICE_DATA.get("536")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("556", Category.Factory.create("Cervejeira", 3, TASKS_SERVICE_DATA.get("536")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("557", Category.Factory.create("Freezer", 3, TASKS_SERVICE_DATA.get("536")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("558", Category.Factory.create("Frigobar", 3, TASKS_SERVICE_DATA.get("536")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("559", Category.Factory.create("Geladeira", 3, TASKS_SERVICE_DATA.get("536")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("560", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("536")!!.documentId).instance)

        /// Forno e Fogões
        TASKS_SERVICE_DATA.put("561", Category.Factory.create("Cooktop", 3, TASKS_SERVICE_DATA.get("537")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("562", Category.Factory.create("Fogão", 3, TASKS_SERVICE_DATA.get("537")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("563", Category.Factory.create("Forno", 3, TASKS_SERVICE_DATA.get("537")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("564", Category.Factory.create("Microondas", 3, TASKS_SERVICE_DATA.get("537")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("565", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("537")!!.documentId).instance)

        /// Eletroportáteis
        TASKS_SERVICE_DATA.put("566", Category.Factory.create("Costura", 3, TASKS_SERVICE_DATA.get("538")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("567", Category.Factory.create("Cozinha", 3, TASKS_SERVICE_DATA.get("538")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("568", Category.Factory.create("Ferro de Passar", 3, TASKS_SERVICE_DATA.get("538")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("569", Category.Factory.create("Limpeza", 3, TASKS_SERVICE_DATA.get("538")!!.documentId).instance)

        /// Lava Louças e Acessórios
        TASKS_SERVICE_DATA.put("570", Category.Factory.create("Lava-Louças", 3, TASKS_SERVICE_DATA.get("539")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("571", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("539")!!.documentId).instance)

        /// Máquinas de Lavar
        TASKS_SERVICE_DATA.put("572", Category.Factory.create("Centrífuga de Roupas", 3, TASKS_SERVICE_DATA.get("540")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("573", Category.Factory.create("Lavadora de Roupas", 3, TASKS_SERVICE_DATA.get("540")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("574", Category.Factory.create("Peças e Acessórios", 3, TASKS_SERVICE_DATA.get("540")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("575", Category.Factory.create("Secadora de Roupas", 3, TASKS_SERVICE_DATA.get("540")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("576", Category.Factory.create("Tanquinho", 3, TASKS_SERVICE_DATA.get("540")!!.documentId).instance)

        /// Peças e Acessórios
        TASKS_SERVICE_DATA.put("577", Category.Factory.create("Para Coifas e Depuradores", 3, TASKS_SERVICE_DATA.get("541")!!.documentId).instance)
    }

    private fun getEletrônicosÁudioVídeo() {
        TASKS_SERVICE_DATA.put("322", Category.Factory.create("Acessórios para Áudio e Vídeo", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("323", Category.Factory.create("Aparelhos DVD e Bluray", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("324", Category.Factory.create("Áudio para Casa", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("325", Category.Factory.create("Áudio Portátil", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("326", Category.Factory.create("Áudio Profissional e DJs", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("327", Category.Factory.create("Bateria, Pilhas e Carregadores", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("328", Category.Factory.create("Calculadoras e Agendas", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("329", Category.Factory.create("Copiadoras e  Acessórios", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("330", Category.Factory.create("Drones e Acessórios", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("331", Category.Factory.create("E-Readers", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("332", Category.Factory.create("Fones de Ouvido", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("333", Category.Factory.create("GPS", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("334", Category.Factory.create("Home Theaters", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("335", Category.Factory.create("iPod", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("336", Category.Factory.create("MP3, MP4 e MP5 Players", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("337", Category.Factory.create("Peças e Componentes Elétricos", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("338", Category.Factory.create("Porta-Retratos Digitais", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("339", Category.Factory.create("Projetores e Telas", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("340", Category.Factory.create("Segurança para Casa", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("341", Category.Factory.create("Suportes", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("342", Category.Factory.create("Tablets e Acessórios", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("343", Category.Factory.create("TV", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("344", Category.Factory.create("TV a Cabo", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("345", Category.Factory.create("Outros Eletrônicos", 3, TASKS_SERVICE_DATA.get("4")!!.documentId).instance)

        /// Acessórios para Áudio e Vídeo
        TASKS_SERVICE_DATA.put("346", Category.Factory.create("Adaptadores", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("347", Category.Factory.create("Antenas de TV", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("348", Category.Factory.create("Cabos", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("349", Category.Factory.create("Controles Remotos", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("350", Category.Factory.create("Conversores e Transcoder", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("351", Category.Factory.create("Fitas de Vídeo", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("352", Category.Factory.create("Fontes de Alimentação", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("353", Category.Factory.create("Media Streaming", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("354", Category.Factory.create("Microfones", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("355", Category.Factory.create("Óculos 3D", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("356", Category.Factory.create("Switches", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("357", Category.Factory.create("Transmissores FM", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("358", Category.Factory.create("Vídeo Cassetes", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("359", Category.Factory.create("Outros", 3, TASKS_SERVICE_DATA.get("322")!!.documentId).instance)

        /// Aparelhos DVD e Bluray
        TASKS_SERVICE_DATA.put("360", Category.Factory.create("DVD Players", 3, TASKS_SERVICE_DATA.get("323")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("361", Category.Factory.create("DVD Players Portáteis", 3, TASKS_SERVICE_DATA.get("323")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("362", Category.Factory.create("Gravadores de DVD", 3, TASKS_SERVICE_DATA.get("323")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("363", Category.Factory.create("Reprodutores de Bluray", 3, TASKS_SERVICE_DATA.get("323")!!.documentId).instance)

        /// Áudio para Casa
        TASKS_SERVICE_DATA.put("364", Category.Factory.create("Amplificadores e Potências", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("365", Category.Factory.create("Caixas Acústicas", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("366", Category.Factory.create("CD Players", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("367", Category.Factory.create("Peças e Componentes", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("368", Category.Factory.create("Receivers", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("369", Category.Factory.create("Sintonizadores / Tuners", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("370", Category.Factory.create("Tape Decks", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("371", Category.Factory.create("Toca Discos", 3, TASKS_SERVICE_DATA.get("324")!!.documentId).instance)

        /// Áudio Portátil
        TASKS_SERVICE_DATA.put("372", Category.Factory.create("Caixas Bluetooth", 3, TASKS_SERVICE_DATA.get("325")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("373", Category.Factory.create("CD Player Portátil", 3, TASKS_SERVICE_DATA.get("325")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("374", Category.Factory.create("Mini Gravador", 3, TASKS_SERVICE_DATA.get("325")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("375", Category.Factory.create("Minidisc", 3, TASKS_SERVICE_DATA.get("325")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("376", Category.Factory.create("Rádios AM/FM", 3, TASKS_SERVICE_DATA.get("325")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("377", Category.Factory.create("Toca-fitas Portátil", 3, TASKS_SERVICE_DATA.get("325")!!.documentId).instance)

        /// Áudio Profissional e DJs
        TASKS_SERVICE_DATA.put("378", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("379", Category.Factory.create("Amplificadores", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("380", Category.Factory.create("Cabos e Adaptadores", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("381", Category.Factory.create("Caixas", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("382", Category.Factory.create("Equalizadores", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("383", Category.Factory.create("Equipamento para DJ", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("384", Category.Factory.create("Gravadores e Placas", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("385", Category.Factory.create("Iluminação", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("386", Category.Factory.create("Máquinas de Fumaça", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("387", Category.Factory.create("Mesas de Som", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("388", Category.Factory.create("Monitores de Áudio", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("389", Category.Factory.create("Pedestais e Suportes", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("390", Category.Factory.create("Potências", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("391", Category.Factory.create("Pré Amplificadores", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("392", Category.Factory.create("Processadores e Racks", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("393", Category.Factory.create("Programas e Softwares de Áudio", 3, TASKS_SERVICE_DATA.get("326")!!.documentId).instance)


        /// Bateria, Pilhas e Carregadores
        TASKS_SERVICE_DATA.put("394", Category.Factory.create("Baterias", 3, TASKS_SERVICE_DATA.get("327")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("395", Category.Factory.create("Carregadores", 3, TASKS_SERVICE_DATA.get("327")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("396", Category.Factory.create("Carregadores com Pilhas", 3, TASKS_SERVICE_DATA.get("327")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("397", Category.Factory.create("Pilhas", 3, TASKS_SERVICE_DATA.get("327")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("398", Category.Factory.create("Testadores", 3, TASKS_SERVICE_DATA.get("327")!!.documentId).instance)

        /// Calculadoras e Agendas
        TASKS_SERVICE_DATA.put("399", Category.Factory.create("Agendas Eletrônicas", 3, TASKS_SERVICE_DATA.get("328")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("400", Category.Factory.create("Calculadoras", 3, TASKS_SERVICE_DATA.get("328")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("401", Category.Factory.create("Científicas", 3, TASKS_SERVICE_DATA.get("328")!!.documentId).instance)

        /// Copiadoras e  Acessórios
        TASKS_SERVICE_DATA.put("402", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("329")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("403", Category.Factory.create("Copiadoras", 3, TASKS_SERVICE_DATA.get("329")!!.documentId).instance)

        /// Drones e Acessórios
        TASKS_SERVICE_DATA.put("404", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("330")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("405", Category.Factory.create("Drone", 3, TASKS_SERVICE_DATA.get("330")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("406", Category.Factory.create("Mini Drone", 3, TASKS_SERVICE_DATA.get("330")!!.documentId).instance)

        /// E-Readers
        TASKS_SERVICE_DATA.put("407", Category.Factory.create("Amazon Kindle", 3, TASKS_SERVICE_DATA.get("331")!!.documentId).instance)
        /// Fones de Ouvido
        TASKS_SERVICE_DATA.put("408", Category.Factory.create("AKG", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("409", Category.Factory.create("Apple", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("410", Category.Factory.create("Arcano", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("411", Category.Factory.create("Awei", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("412", Category.Factory.create("Beats", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("413", Category.Factory.create("Bluedio", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("414", Category.Factory.create("Bomber", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("415", Category.Factory.create("Bose", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("416", Category.Factory.create("Coby", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("417", Category.Factory.create("Fortrek", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("418", Category.Factory.create("iSound", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("419", Category.Factory.create("Jaybird", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("420", Category.Factory.create("JBL", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("421", Category.Factory.create("Jvc", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("422", Category.Factory.create("Knup", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("423", Category.Factory.create("Kolke", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("424", Category.Factory.create("Koss", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("425", Category.Factory.create("Leadership", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("426", Category.Factory.create("Logitech", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("427", Category.Factory.create("Marshall", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("428", Category.Factory.create("Maxell", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("429", Category.Factory.create("Motorola", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("430", Category.Factory.create("Moxpad", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("431", Category.Factory.create("Multilaser", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("432", Category.Factory.create("Oex", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("433", Category.Factory.create("Panasonic", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("434", Category.Factory.create("Philips", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("435", Category.Factory.create("Pioneer", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("436", Category.Factory.create("Samsung", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("437", Category.Factory.create("Sennheiser", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("438", Category.Factory.create("Shure", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("439", Category.Factory.create("Skullcandy", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("440", Category.Factory.create("Sony", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("441", Category.Factory.create("Superlux", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("442", Category.Factory.create("Technica", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("443", Category.Factory.create("Vinik", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("444", Category.Factory.create("Vivitar", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("445", Category.Factory.create("Waldman", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("446", Category.Factory.create("Xiaomi", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("447", Category.Factory.create("Outras Marcas", 3, TASKS_SERVICE_DATA.get("332")!!.documentId).instance)

        /// GPS
        TASKS_SERVICE_DATA.put("448", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("333")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("449", Category.Factory.create("Aparelhos", 3, TASKS_SERVICE_DATA.get("333")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("450", Category.Factory.create("Mapas e Cartas Náuticas", 3, TASKS_SERVICE_DATA.get("333")!!.documentId).instance)

        /// Home Theaters
        TASKS_SERVICE_DATA.put("451", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("334")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("452", Category.Factory.create("Caixas Acústicas", 3, TASKS_SERVICE_DATA.get("334")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("453", Category.Factory.create("Home Theaters", 3, TASKS_SERVICE_DATA.get("334")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("454", Category.Factory.create("Receivers", 3, TASKS_SERVICE_DATA.get("334")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("455", Category.Factory.create("Subwoofers", 3, TASKS_SERVICE_DATA.get("334")!!.documentId).instance)

        /// iPod
        TASKS_SERVICE_DATA.put("456", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("335")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("457", Category.Factory.create("Reprodutores", 3, TASKS_SERVICE_DATA.get("335")!!.documentId).instance)

        /// MP3, MP4 e MP5 Players
        TASKS_SERVICE_DATA.put("458", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("336")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("459", Category.Factory.create("iPod", 3, TASKS_SERVICE_DATA.get("336")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("460", Category.Factory.create("MP3", 3, TASKS_SERVICE_DATA.get("336")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("461", Category.Factory.create("MP4", 3, TASKS_SERVICE_DATA.get("336")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("462", Category.Factory.create("MP5 - Com Câmera", 3, TASKS_SERVICE_DATA.get("336")!!.documentId).instance)

        /// Peças e Componentes Elétricos
        TASKS_SERVICE_DATA.put("463", Category.Factory.create("Abracadeira", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("464", Category.Factory.create("Adaptadores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("465", Category.Factory.create("Barras de Led", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("466", Category.Factory.create("Cabo Flat", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("467", Category.Factory.create("Capacitores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("468", Category.Factory.create("Chaves", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("469", Category.Factory.create("Circuitos Integrados", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("470", Category.Factory.create("Condensador", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("471", Category.Factory.create("Conectores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("472", Category.Factory.create("Diodo", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("473", Category.Factory.create("Displays e LCD", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("474", Category.Factory.create("Economizadores de Energia", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("475", Category.Factory.create("Estações de Solda", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("476", Category.Factory.create("Ferros de Solda", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("477", Category.Factory.create("Flybacks", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("478", Category.Factory.create("Fusivel", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("479", Category.Factory.create("Induzido", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("480", Category.Factory.create("Leds", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("481", Category.Factory.create("Microcontroladores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("482", Category.Factory.create("Módulos", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("483", Category.Factory.create("Placas da Fonte TV", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("484", Category.Factory.create("Placas Main", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("485", Category.Factory.create("Potenciômetros", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("486", Category.Factory.create("Relés", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("487", Category.Factory.create("Resistores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("488", Category.Factory.create("Sensores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("489", Category.Factory.create("Transformadores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("490", Category.Factory.create("Transistores", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("491", Category.Factory.create("Unidades Ópticas", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("492", Category.Factory.create("Válvulas", 3, TASKS_SERVICE_DATA.get("337")!!.documentId).instance)

        /// Porta-Retratos Digitais
        TASKS_SERVICE_DATA.put("493", Category.Factory.create("Chaveiros", 3, TASKS_SERVICE_DATA.get("338")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("494", Category.Factory.create("Porta-Retratos", 3, TASKS_SERVICE_DATA.get("338")!!.documentId).instance)

        /// Projetores e Telas
        TASKS_SERVICE_DATA.put("495", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("339")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("496", Category.Factory.create("Projetores", 3, TASKS_SERVICE_DATA.get("339")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("497", Category.Factory.create("Suporte", 3, TASKS_SERVICE_DATA.get("339")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("498", Category.Factory.create("Telas", 3, TASKS_SERVICE_DATA.get("339")!!.documentId).instance)

        /// Segurança para Casa
        TASKS_SERVICE_DATA.put("499", Category.Factory.create("Alarmes", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("500", Category.Factory.create("Cadeados", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("501", Category.Factory.create("Câmeras de Segurança", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("502", Category.Factory.create("Campainhas", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("503", Category.Factory.create("Cercas de Segurança", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("504", Category.Factory.create("Cofres", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("505", Category.Factory.create("Controles Remotos para Alarmes", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("506", Category.Factory.create("Fechaduras", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("507", Category.Factory.create("Kit de Monitoramento", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("508", Category.Factory.create("Motor para Portão Eletrônico", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("509", Category.Factory.create("Porteiros", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("510", Category.Factory.create("Redes de Proteção", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("511", Category.Factory.create("Travas de Segurança", 3, TASKS_SERVICE_DATA.get("440")!!.documentId).instance)

        /// Suportes
        TASKS_SERVICE_DATA.put("512", Category.Factory.create("DVD", 3, TASKS_SERVICE_DATA.get("441")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("513", Category.Factory.create("Home Theater", 3, TASKS_SERVICE_DATA.get("441")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("514", Category.Factory.create("Parlantes", 3, TASKS_SERVICE_DATA.get("441")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("515", Category.Factory.create("TV", 3, TASKS_SERVICE_DATA.get("441")!!.documentId).instance)

        /// Tablets e Acessórios
        TASKS_SERVICE_DATA.put("516", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("442")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("517", Category.Factory.create("E-readers", 3, TASKS_SERVICE_DATA.get("442")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("518", Category.Factory.create("Peças", 3, TASKS_SERVICE_DATA.get("442")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("519", Category.Factory.create("Tablets", 3, TASKS_SERVICE_DATA.get("442")!!.documentId).instance)

        /// TV
        TASKS_SERVICE_DATA.put("520", Category.Factory.create("4K", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("521", Category.Factory.create("Mini TVs e Portáteis", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("522", Category.Factory.create("Smart TV", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("523", Category.Factory.create("TV 3D", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("524", Category.Factory.create("TV de Tubo", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("525", Category.Factory.create("TV LCD", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("526", Category.Factory.create("TV LED", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("527", Category.Factory.create("TV Plasma", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("528", Category.Factory.create("Outras TVs", 3, TASKS_SERVICE_DATA.get("443")!!.documentId).instance)

        /// TV a Cabo
        TASKS_SERVICE_DATA.put("529", Category.Factory.create("Antenas Parabólicas e Kits", 3, TASKS_SERVICE_DATA.get("444")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("530", Category.Factory.create("Controles Remotos", 3, TASKS_SERVICE_DATA.get("444")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("531", Category.Factory.create("Localizadores de Satélite", 3, TASKS_SERVICE_DATA.get("444")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("532", Category.Factory.create("Receptores", 3, TASKS_SERVICE_DATA.get("444")!!.documentId).instance)

        /// Outros Eletrônicos
        TASKS_SERVICE_DATA.put("533", Category.Factory.create("Eletrônicos Vintage", 3, TASKS_SERVICE_DATA.get("445")!!.documentId).instance)
    }

    private fun getBrinquedosHobbies() {
        TASKS_SERVICE_DATA.put("187", Category.Factory.create("Ar Livre, Malabares e Festas", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("188", Category.Factory.create("Bonecas e Acessórios", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("189", Category.Factory.create("Bonecos e Figuras de Ação", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("190", Category.Factory.create("Brinquedos", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("191", Category.Factory.create("Brinquedos de Controle Remoto", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("192", Category.Factory.create("Brinquedos para Bebês", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("193", Category.Factory.create("Cards e Card Games", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("194", Category.Factory.create("Filmes Infantis", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("195", Category.Factory.create("Lego e Blocos de Montar", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("196", Category.Factory.create("Jogos", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("197", Category.Factory.create("Mini Veículos e Bicicletas", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("198", Category.Factory.create("Modelismo Profissional", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("199", Category.Factory.create("Música Infantil", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("200", Category.Factory.create("Pelúcias", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("201", Category.Factory.create("Veículos em Miniatura", 2, TASKS_SERVICE_DATA.get("3")!!.documentId).instance)

        //Ar Livre, Malabares e Festas
        TASKS_SERVICE_DATA.put("202", Category.Factory.create("Balanços", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("203", Category.Factory.create("Brinquedos para Playground", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("204", Category.Factory.create("Camas Elásticas e Acessórios", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("205", Category.Factory.create("Casinhas", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("206", Category.Factory.create("Festas", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("207", Category.Factory.create("Malabares", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("208", Category.Factory.create("Piscinas e Infláveis", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("209", Category.Factory.create("Outros", 3, TASKS_SERVICE_DATA.get("187")!!.documentId).instance)

        ///Bonecas e Acessórios
        TASKS_SERVICE_DATA.put("210", Category.Factory.create("Acessórios para Bonecas", 3, TASKS_SERVICE_DATA.get("188")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("211", Category.Factory.create("Bonecas", 3, TASKS_SERVICE_DATA.get("188")!!.documentId).instance)

        ///Bonecos e Figuras de Ação
        TASKS_SERVICE_DATA.put("212", Category.Factory.create("Anime", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("213", Category.Factory.create("Forte Apache", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("214", Category.Factory.create("G.I.Joe / Comandos em Ação", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("215", Category.Factory.create("Mage Knight / Figuras R.P.G.", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("216", Category.Factory.create("MechWarrior", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("210", Category.Factory.create("Meu Querido Pônei", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("211", Category.Factory.create("Miniaturas Kinder Ovo e Afins", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("212", Category.Factory.create("Músicos", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("213", Category.Factory.create("Personagens de Games", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("214", Category.Factory.create("Personagens do Cinema e da TV", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("215", Category.Factory.create("Personagens Esportivos", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("216", Category.Factory.create("Playmobil", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("217", Category.Factory.create("Soldadinhos", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("218", Category.Factory.create("Spawn", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("219", Category.Factory.create("Super Heróis", 3, TASKS_SERVICE_DATA.get("189")!!.documentId).instance)

        ///Brinquedos
        TASKS_SERVICE_DATA.put("220", Category.Factory.create("Antigos", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("221", Category.Factory.create("Armas de Brinquedo", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("222", Category.Factory.create("Brinquedos com Choque", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("223", Category.Factory.create("Carrinhos e Veículos", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("224", Category.Factory.create("Eletrodomésticos e Móveis", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("225", Category.Factory.create("Fantoches", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("226", Category.Factory.create("Hand Spinner", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("227", Category.Factory.create("Instrumentos Musicais", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("228", Category.Factory.create("Laptops", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("229", Category.Factory.create("Pipas e Acessórios", 3, TASKS_SERVICE_DATA.get("190")!!.documentId).instance)

        ///Brinquedos de Controle Remoto
        TASKS_SERVICE_DATA.put("230", Category.Factory.create("Aviões", 3, TASKS_SERVICE_DATA.get("191")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("231", Category.Factory.create("Carros", 3, TASKS_SERVICE_DATA.get("191")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("232", Category.Factory.create("Helicópteros", 3, TASKS_SERVICE_DATA.get("191")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("233", Category.Factory.create("Lanchas e Barcos", 3, TASKS_SERVICE_DATA.get("191")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("234", Category.Factory.create("Pick Ups, Jipes e Caminhões", 3, TASKS_SERVICE_DATA.get("191")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("235", Category.Factory.create("Robôs", 3, TASKS_SERVICE_DATA.get("191")!!.documentId).instance)


        ///Brinquedos para Bebês
        TASKS_SERVICE_DATA.put("236", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("237", Category.Factory.create("Andadores", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("238", Category.Factory.create("Bonecas", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("239", Category.Factory.create("Bonecos de Apertar", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("240", Category.Factory.create("Cadeiras de Descanso", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("241", Category.Factory.create("Centros de Atividades", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("242", Category.Factory.create("Ginásios", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("243", Category.Factory.create("Joguinhos", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("244", Category.Factory.create("Jumper de Atividades", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("245", Category.Factory.create("Móbiles", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("246", Category.Factory.create("Mordedores", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("247", Category.Factory.create("Musicais", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("248", Category.Factory.create("Pelúcias", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("249", Category.Factory.create("Piscinas para Bebês", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("250", Category.Factory.create("Tapetes", 3, TASKS_SERVICE_DATA.get("192")!!.documentId).instance)

        ///Cards e Card Games
        TASKS_SERVICE_DATA.put("251", Category.Factory.create("Card Games", 3, TASKS_SERVICE_DATA.get("193")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("252", Category.Factory.create("Cards", 3, TASKS_SERVICE_DATA.get("193")!!.documentId).instance)

        ///"Filmes Infantis
        TASKS_SERVICE_DATA.put("253", Category.Factory.create("Blu-ray", 3, TASKS_SERVICE_DATA.get("194")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("254", Category.Factory.create("DVDs", 3, TASKS_SERVICE_DATA.get("194")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("255", Category.Factory.create("Laserdiscs", 3, TASKS_SERVICE_DATA.get("194")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("256", Category.Factory.create("VHS", 3, TASKS_SERVICE_DATA.get("194")!!.documentId).instance)

        ///Lego e Blocos de Montar
        TASKS_SERVICE_DATA.put("257", Category.Factory.create("Bonecos", 3, TASKS_SERVICE_DATA.get("195")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("258", Category.Factory.create("Lotes de Peças", 3, TASKS_SERVICE_DATA.get("195")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("259", Category.Factory.create("Placas", 3, TASKS_SERVICE_DATA.get("195")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("260", Category.Factory.create("Sets Completos", 3, TASKS_SERVICE_DATA.get("195")!!.documentId).instance)

        /// Jogos
        TASKS_SERVICE_DATA.put("261", Category.Factory.create("Bingo", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("262", Category.Factory.create("Cubo Mágico", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("263", Category.Factory.create("Dominó", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("264", Category.Factory.create("Futebol de Botão", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("265", Category.Factory.create("Jogo de Dardos", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("266", Category.Factory.create("Jogos de Tabuleiro", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("267", Category.Factory.create("Jogos Educativos", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("268", Category.Factory.create("Jogos Eletrônicos", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("269", Category.Factory.create("Jogos R.P.G.", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("270", Category.Factory.create("Mágicas e Pegadinhas", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("271", Category.Factory.create("Pebolim", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("272", Category.Factory.create("Poker", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("273", Category.Factory.create("Quebra Cabeças", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("288", Category.Factory.create("Snooker / Sinuca", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("289", Category.Factory.create("Xadrez", 3, TASKS_SERVICE_DATA.get("196")!!.documentId).instance)

        ///Mini Veículos e Bicicletas
        TASKS_SERVICE_DATA.put("290", Category.Factory.create("Acessórios e Peças", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("291", Category.Factory.create("Bicicletas Infantis", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("292", Category.Factory.create("Carros e Jeeps", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("293", Category.Factory.create("Hoverboard", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("294", Category.Factory.create("Mini-Motos", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("295", Category.Factory.create("Scooters e Patinetes", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("296", Category.Factory.create("Triciclos", 3, TASKS_SERVICE_DATA.get("197")!!.documentId).instance)

        ///Modelismo Profissional
        TASKS_SERVICE_DATA.put("297", Category.Factory.create("Aerógrafos", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("298", Category.Factory.create("Aeromodelismo", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("299", Category.Factory.create("Automodelismo", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("300", Category.Factory.create("Autorama e Slot Cars", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("301", Category.Factory.create("Ferromodelismo", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("302", Category.Factory.create("Modelismo Naval", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("303", Category.Factory.create("Plastimodelismo", 3, TASKS_SERVICE_DATA.get("198")!!.documentId).instance)

        ///Música Infantil
        TASKS_SERVICE_DATA.put("304", Category.Factory.create("Cassetes", 3, TASKS_SERVICE_DATA.get("199")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("305", Category.Factory.create("CDs", 3, TASKS_SERVICE_DATA.get("199")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("306", Category.Factory.create("DVDs", 3, TASKS_SERVICE_DATA.get("199")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("307", Category.Factory.create("Vinil e LPs", 3, TASKS_SERVICE_DATA.get("199")!!.documentId).instance)

        ///Pelúcias
        TASKS_SERVICE_DATA.put("308", Category.Factory.create("Animais", 3, TASKS_SERVICE_DATA.get("200")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("309", Category.Factory.create("Personagens", 3, TASKS_SERVICE_DATA.get("200")!!.documentId).instance)

        //Veículos em Miniatura
        TASKS_SERVICE_DATA.put("310", Category.Factory.create("Automóveis", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("311", Category.Factory.create("Aviões", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("312", Category.Factory.create("Barcos", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("313", Category.Factory.create("Caminhões", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("314", Category.Factory.create("Caminhonetas e Pickups", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("315", Category.Factory.create("Helicópteros", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("316", Category.Factory.create("Militares", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("317", Category.Factory.create("Mini Estantes", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("318", Category.Factory.create("Motos", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("319", Category.Factory.create("Ônibus", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("320", Category.Factory.create("Tratores e Veículos Agrícolas", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("321", Category.Factory.create("Trens", 3, TASKS_SERVICE_DATA.get("201")!!.documentId).instance)
    }

    private fun getAlimentosBebidas() {
        TASKS_SERVICE_DATA.put("82", Category.Factory.create("Alimentos para Bebês", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("83", Category.Factory.create("Cachaça", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("84", Category.Factory.create("Café", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("85", Category.Factory.create("Cerveja", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("86", Category.Factory.create("Champagnes", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("87", Category.Factory.create("Comestíveis", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("88", Category.Factory.create("Infusão", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("89", Category.Factory.create("Licor", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("90", Category.Factory.create("Vinhos", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("91", Category.Factory.create("Whisky", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("92", Category.Factory.create("Vodka", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("93", Category.Factory.create("Tequila", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("94", Category.Factory.create("Suco", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("95", Category.Factory.create("Gin", 2, TASKS_SERVICE_DATA.get("2")!!.documentId).instance)

        // Alimentos para Bebês - 3
        TASKS_SERVICE_DATA.put("95", Category.Factory.create("Leite Infantil", 3, TASKS_SERVICE_DATA.get("82")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("96", Category.Factory.create("Papinhas", 3, TASKS_SERVICE_DATA.get("82")!!.documentId).instance)

        // Cachaça - 3
        TASKS_SERVICE_DATA.put("96", Category.Factory.create("Adega Da Pinga", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("97", Category.Factory.create("Boazinha", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("98", Category.Factory.create("Chico Mineiro", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("99", Category.Factory.create("Chico Valim", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("100", Category.Factory.create("Jerominho Ribeiro", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("101", Category.Factory.create("Joao Mendes", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("102", Category.Factory.create("Lua Azul", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("103", Category.Factory.create("Sabor De Minas", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("104", Category.Factory.create("Santa Rosa", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("105", Category.Factory.create("Santo Grau", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("106", Category.Factory.create("Vale Verde", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("107", Category.Factory.create("Weber Haus", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("108", Category.Factory.create("Werneck", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("109", Category.Factory.create("Ypioca", 3, TASKS_SERVICE_DATA.get("83")!!.documentId).instance)

        // CAfé
        TASKS_SERVICE_DATA.put("110", Category.Factory.create("Café em Cápsulas", 3, TASKS_SERVICE_DATA.get("84")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("111", Category.Factory.create("Café em Grão", 3, TASKS_SERVICE_DATA.get("84")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("112", Category.Factory.create("Café Moído", 3, TASKS_SERVICE_DATA.get("84")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("113", Category.Factory.create("Café Solúvel", 3, TASKS_SERVICE_DATA.get("84")!!.documentId).instance)

        // CErveja
        TASKS_SERVICE_DATA.put("115", Category.Factory.create("Artesanal", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("116", Category.Factory.create("Ashby", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("117", Category.Factory.create("Brahma", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("118", Category.Factory.create("Erdinger", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("119", Category.Factory.create("Estrella", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("120", Category.Factory.create("Heineken", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("121", Category.Factory.create("Opa Bier", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("122", Category.Factory.create("Paulaner", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("123", Category.Factory.create("Schneider Weisse Tap", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("125", Category.Factory.create("Skol", 3, TASKS_SERVICE_DATA.get("85")!!.documentId).instance)

        // Champagnes
        TASKS_SERVICE_DATA.put("125", Category.Factory.create("Chandon", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("126", Category.Factory.create("Dom Perignon", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("127", Category.Factory.create("Freixenet", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("128", Category.Factory.create("Giaretta", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("129", Category.Factory.create("Jp Chenet", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("130", Category.Factory.create("Louis Roederer", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("131", Category.Factory.create("Moscatel", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("132", Category.Factory.create("Mumm", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("133", Category.Factory.create("Perrier Jouet", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("134", Category.Factory.create("Salton", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("135", Category.Factory.create("Taittinger", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("136", Category.Factory.create("Terranova", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("137", Category.Factory.create("Veuve Clicquot", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("138", Category.Factory.create("Veuve Du Vernay", 3, TASKS_SERVICE_DATA.get("86")!!.documentId).instance)

        // Comestíveis
        TASKS_SERVICE_DATA.put("140", Category.Factory.create("Acucar Mascavo", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("141", Category.Factory.create("Adocante", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("142", Category.Factory.create("Arroz", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("143", Category.Factory.create("Azeite", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("144", Category.Factory.create("Azeite De Oliva", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("145", Category.Factory.create("Batata", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("146", Category.Factory.create("Cachaca", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("147", Category.Factory.create("Cafe", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("148", Category.Factory.create("Castanha De Caju", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("149", Category.Factory.create("Chiclete", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("150", Category.Factory.create("Chocolate", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("151", Category.Factory.create("Creme De Avela", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("152", Category.Factory.create("Doce De Leite", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("153", Category.Factory.create("Doces", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("154", Category.Factory.create("Erva Mate", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("155", Category.Factory.create("Farinha", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("156", Category.Factory.create("Mel", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("157", Category.Factory.create("Molho De Pimenta", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("158", Category.Factory.create("Oleo De Coco", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("159", Category.Factory.create("Papel Arroz", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("160", Category.Factory.create("Pimenta", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("161", Category.Factory.create("Pirulito", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("162", Category.Factory.create("Sal Marinho", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("163", Category.Factory.create("Sal Rosa", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("164", Category.Factory.create("Salgadinho", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("165", Category.Factory.create("Tempero", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("166", Category.Factory.create("Vinho", 3, TASKS_SERVICE_DATA.get("87")!!.documentId).instance)

        // Infusão
        TASKS_SERVICE_DATA.put("167", Category.Factory.create("Chá", 3, TASKS_SERVICE_DATA.get("88")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("168", Category.Factory.create("Erva-mate", 3, TASKS_SERVICE_DATA.get("88")!!.documentId).instance)

        // Licor
        TASKS_SERVICE_DATA.put("169", Category.Factory.create("Livros de culinária", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("170", Category.Factory.create("Bebidas", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("171", Category.Factory.create("Congelados e Microondas", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("172", Category.Factory.create("Culinária Internacional", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("173", Category.Factory.create("Culinária Nacional", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("174", Category.Factory.create("Diet e Light", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("175", Category.Factory.create("Doces e Sobremesas", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("176", Category.Factory.create("Natural e Vegetariana", 3, TASKS_SERVICE_DATA.get("89")!!.documentId).instance)

        // Vinhos
        TASKS_SERVICE_DATA.put("177", Category.Factory.create("Acesórios", 3, TASKS_SERVICE_DATA.get("90")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("178", Category.Factory.create("Vinhos", 3, TASKS_SERVICE_DATA.get("90")!!.documentId).instance)

        // Whisky
        TASKS_SERVICE_DATA.put("179", Category.Factory.create("Ballantines", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("180", Category.Factory.create("Chivas Regal", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("181", Category.Factory.create("Glenfiddich", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("182", Category.Factory.create("Glenlivet", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("183", Category.Factory.create("Jack Daniels", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("184", Category.Factory.create("Jim Bean", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("185", Category.Factory.create("Johnnie Walker", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("186", Category.Factory.create("Old Parr", 3, TASKS_SERVICE_DATA.get("91")!!.documentId).instance)
    }

    private fun getAcessoriosVeiculos() {
        TASKS_SERVICE_DATA.put("6", Category.Factory.create("Acessórios de Carros", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("7", Category.Factory.create("Acessórios de Motos", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("8", Category.Factory.create("Acessórios Náutica", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("9", Category.Factory.create("GPS", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("10", Category.Factory.create("Peças Automotivas", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("11", Category.Factory.create("Peças de Moto", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("12", Category.Factory.create("Pneus", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("13", Category.Factory.create("Rodas", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("14", Category.Factory.create("Som Automotivo", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("15", Category.Factory.create("Tuning e Performance", 2, TASKS_SERVICE_DATA.get("1")!!.documentId).instance)

        //Acessórios de Carros
        TASKS_SERVICE_DATA.put("16", Category.Factory.create("Calotas", 3, TASKS_SERVICE_DATA.get("6")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("17", Category.Factory.create("Exterior", 3, TASKS_SERVICE_DATA.get("6")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("18", Category.Factory.create("Interior", 3, TASKS_SERVICE_DATA.get("6")!!.documentId).instance)

        // Acessórios de Motos
        TASKS_SERVICE_DATA.put("19", Category.Factory.create("Acabamento", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("20", Category.Factory.create("Alarmes", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("21", Category.Factory.create("Bagageiros", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("22", Category.Factory.create("Baús e Mochilas", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("23", Category.Factory.create("Capacetes", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("24", Category.Factory.create("Capas para Motos", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("25", Category.Factory.create("Intercomunicadores", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("26", Category.Factory.create("Roupa de Moto", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("27", Category.Factory.create("Suportes", 3, TASKS_SERVICE_DATA.get("7")!!.documentId).instance)

        //Acessórios Náutica
        TASKS_SERVICE_DATA.put("28", Category.Factory.create("Âncoras", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("29", Category.Factory.create("Bombas", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("30", Category.Factory.create("Cabos", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("31", Category.Factory.create("Hélices", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("32", Category.Factory.create("Iluminação", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("33", Category.Factory.create("Inversores", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("34", Category.Factory.create("Motores", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("35", Category.Factory.create("Rotores", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("36", Category.Factory.create("Sonares e GPS", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("37", Category.Factory.create("Velas", 3, TASKS_SERVICE_DATA.get("8")!!.documentId).instance)

        //GPS
        TASKS_SERVICE_DATA.put("38", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("9")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("39", Category.Factory.create("Aparelhos", 3, TASKS_SERVICE_DATA.get("9")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("40", Category.Factory.create("Mapas e Cartas Náuticas", 3, TASKS_SERVICE_DATA.get("9")!!.documentId).instance)

        //Peças Automotivas
        TASKS_SERVICE_DATA.put("40", Category.Factory.create("Carroceria", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("41", Category.Factory.create("Climatização", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("42", Category.Factory.create("Eletroventiladores", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("43", Category.Factory.create("Escapamentos", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("44", Category.Factory.create("Fechaduras e Chaves", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("45", Category.Factory.create("Filtros", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("46", Category.Factory.create("Freios", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("47", Category.Factory.create("Ignição", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("48", Category.Factory.create("Iluminação", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("49", Category.Factory.create("Injeção", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("50", Category.Factory.create("Motor", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("51", Category.Factory.create("Peças de Exterior", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("52", Category.Factory.create("Peças de Interior", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("53", Category.Factory.create("Segurança", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("54", Category.Factory.create("Suspensão e Direção", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("55", Category.Factory.create("Transmissão", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("56", Category.Factory.create("Vidros", 3, TASKS_SERVICE_DATA.get("10")!!.documentId).instance)

        //Peças de Moto
        TASKS_SERVICE_DATA.put("57", Category.Factory.create("Chassis", 3, TASKS_SERVICE_DATA.get("11")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("58", Category.Factory.create("Motores e Partes", 3, TASKS_SERVICE_DATA.get("11")!!.documentId).instance)

        // Pneus
        TASKS_SERVICE_DATA.put("59", Category.Factory.create("Pneus para Carros", 3, TASKS_SERVICE_DATA.get("12")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("59", Category.Factory.create("Pneus para Motos", 3, TASKS_SERVICE_DATA.get("12")!!.documentId).instance)

        // Rodas
        TASKS_SERVICE_DATA.put("60", Category.Factory.create("Rodas para Carros", 3, TASKS_SERVICE_DATA.get("13")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("61", Category.Factory.create("Rodas para Motos", 3, TASKS_SERVICE_DATA.get("13")!!.documentId).instance)// Rodas

        // Som Automotivo
        TASKS_SERVICE_DATA.put("62", Category.Factory.create("Acessórios", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("63", Category.Factory.create("Alto-falantes e Woofers", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("64", Category.Factory.create("CD Players", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("65", Category.Factory.create("Central Multimídia", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("66", Category.Factory.create("Controles Remotos", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("67", Category.Factory.create("DVD Player Automotivo", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("68", Category.Factory.create("Equalizadores e Crossovers", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("69", Category.Factory.create("Módulos Amplificadores", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("70", Category.Factory.create("Rádio Automotivo", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("71", Category.Factory.create("Telas Automotivas", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)
        TASKS_SERVICE_DATA.put("72", Category.Factory.create("Toca-fitas", 3, TASKS_SERVICE_DATA.get("14")!!.documentId).instance)

        // Tuning e Performance
        TASKS_SERVICE_DATA.put("73", Category.Factory.create("Acessórios Cromados", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("74", Category.Factory.create("Adesivos", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("75", Category.Factory.create("Iluminação Tuning", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("76", Category.Factory.create("Maçanetas", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("77", Category.Factory.create("Painéis", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("78", Category.Factory.create("Pedaleiras e Soleiras", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("79", Category.Factory.create("Performance", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("80", Category.Factory.create("Tuning Exterior", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
        TASKS_SERVICE_DATA.put("81", Category.Factory.create("Tuning Interior", 3, TASKS_SERVICE_DATA.get("15")!!.documentId, true).instance)
    }

    fun get(documentId: String): Category? = TASKS_SERVICE_DATA[documentId]

    companion object {

        private lateinit var INSTANCE: FakeCategoryRemoteDataSource
        private var needsNewInstance = true

        @JvmStatic
        fun getInstance(): FakeCategoryRemoteDataSource {
            if (needsNewInstance) {
                INSTANCE = FakeCategoryRemoteDataSource()
                needsNewInstance = false
            }
            return INSTANCE
        }
    }
    */
}