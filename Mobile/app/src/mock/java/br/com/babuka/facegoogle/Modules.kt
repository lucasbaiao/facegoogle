package br.com.babuka.facegoogle

import br.com.babuka.couchbase.repository.CouchbaseCategoriasRepository
import br.com.babuka.couchbase.repository.CouchbasePostsRepository
import br.com.babuka.domain.interfaces.repositories.CategoriaDataSource
import br.com.babuka.domain.interfaces.repositories.PostsDataSource
import org.koin.dsl.module.applicationContext


val RepositoryModule = applicationContext {


    //bean("couchbasePostsRepository") { CouchbasePostsRepository(get()) }
    provide { CouchbasePostsRepository(get()) } bind PostsDataSource::class

    //bean("categoryRemoteDataSource") { FakeCategoryRemoteDataSource() }
    //bean("categoryLocalDataSource") { CategoryLocalDataSource(get()) }
    provide { CouchbaseCategoriasRepository(get()) } bind (CategoriaDataSource::class)
}