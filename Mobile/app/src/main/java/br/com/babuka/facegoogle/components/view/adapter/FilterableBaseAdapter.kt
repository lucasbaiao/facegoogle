package br.com.babuka.facegoogle.components.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.babuka.domain.interfaces.components.FilterableModel
import com.google.common.collect.Lists

abstract class FilterableBaseAdapter<T : FilterableModel, H : FilterableBaseAdapter.ViewHolder> private constructor() : RecyclerView.Adapter<H>() {

    private lateinit var dataSet: ArrayList<T>
    protected lateinit var publishResults: ArrayList<T>

    abstract fun bindView(item: T, holder: H)

    constructor(dataSet: ArrayList<T>) : this() {
        this.publishResults = Lists.newArrayList(dataSet)
        this.dataSet = dataSet
    }

    override fun onBindViewHolder(holder: H, position: Int) {
        val item = publishResults[position]
        bindView(item, holder)
    }

    override fun getItemCount(): Int = publishResults.size

    override fun getItemId(i: Int) = i.toLong()

    /**
     * Filter Logic
     */
    fun filter(query: String) {
        val filteredModelList = ArrayList<T>()
        for (model in dataSet) {
            model.filterProperties.forEach { text ->
                if (query.isEmpty() || text.contains(query, true)) {
                    filteredModelList.add(model)
                }
            }
        }

        animateTo(filteredModelList)
    }

    private fun animateTo(models: List<T>) {
        applyAndAnimateRemovals(models)
        applyAndAnimateAdditions(models)
        applyAndAnimateMovedItems(models)
    }

    private fun applyAndAnimateRemovals(newModels: List<T>) {
        for (i in publishResults.size - 1 downTo 0) {
            val model = publishResults.get(i)
            if (!newModels.contains(model)) {
                removeItem(i)
            }
        }
    }

    private fun applyAndAnimateAdditions(newModels: List<T>) {
        var i = 0
        val count = newModels.size
        while (i < count) {
            val model = newModels[i]
            if (!publishResults.contains(model)) {
                addItem(i, model)
            }
            i++
        }
    }

    private fun applyAndAnimateMovedItems(newModels: List<T>) {
        for (toPosition in newModels.indices.reversed()) {
            val model = newModels[toPosition]
            val fromPosition = publishResults.indexOf(model)
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition)
            }
        }
    }

    private fun removeItem(position: Int): T {
        val model = publishResults.removeAt(position)
        notifyItemRemoved(position)
        return model
    }

    private fun moveItem(fromPosition: Int, toPosition: Int) {
        val model = publishResults.removeAt(fromPosition)
        publishResults.add(toPosition, model)
        notifyItemMoved(fromPosition, toPosition)
    }

    private fun addItem(position: Int, model: T) {
        publishResults.add(position, model)
        notifyItemInserted(position)
    }

    fun addItem(model: T) {
        addItem(publishResults.size, model)
        dataSet.add(model)
    }

    fun clear() {
        dataSet.clear()
        publishResults.clear()
        notifyDataSetChanged()
    }

    abstract class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)
}