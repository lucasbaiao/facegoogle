package br.com.babuka.facegoogle.view.categorias

import br.com.babuka.domain.entities.Category
import br.com.babuka.facegoogle.view.BasePresenter
import br.com.babuka.facegoogle.view.BaseView


interface CategoriesContract {

    interface View : BaseView<Presenter> {

        var isActive: Boolean
        fun setLoadingIndicator(isVisible: Boolean)
        fun showLoadingError()
        fun showData(data: List<Category>)
        fun showNoData()
        fun setTitle(title: String)
        fun doBack(): Boolean
    }

    interface Presenter : BasePresenter<View> {
        fun loadCategories(forceUpdate: Boolean)
        fun showCategoryChildren(clickedItem: Category)
        fun showPreviousLevel(): Boolean
    }
}