package br.com.babuka.facegoogle.di

import br.com.babuka.facegoogle.RepositoryModule
import br.com.babuka.facegoogle.di.Context.Categories
import br.com.babuka.facegoogle.di.Properties.EXTRA_TASK_ID
import br.com.babuka.facegoogle.view.categorias.CategoriesContract
import br.com.babuka.facegoogle.view.categorias.CategoriesFragment
import br.com.babuka.facegoogle.view.categorias.CategoriesPresenter
import br.com.babuka.facegoogle.view.post.PostsContract
import br.com.babuka.facegoogle.view.post.PostsFragment
import br.com.babuka.facegoogle.view.post.PostsPresenter
import org.koin.dsl.module.applicationContext

/**
 * Koin main module
 */
val AppModule = applicationContext {

    factory { PostsFragment() }
    factory { PostsPresenter(get()) as PostsContract.Presenter }

    factory { CategoriesFragment() }
    factory { CategoriesPresenter(getProperty(EXTRA_TASK_ID, ""),get()) as CategoriesContract.Presenter }

}


/**
 * Module list
 */
val todoAppModules = listOf(RepositoryModule, AppModule)


object Properties {
    const val EXTRA_TASK_ID = "TASK_ID"
}


/**
 * Module constants
 */
object Context {
    val Login = "Login"
    val Posts = "Posts"
    val Categories = "Categories"
}