package br.com.babuka.facegoogle.components.widgets

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.babuka.facegoogle.R


class DividerItemDecoration constructor(private val mDivider: Drawable?) : RecyclerView.ItemDecoration() {

    constructor(context: Context) : this(ContextCompat.getDrawable(context, R.drawable.divider_decorator)) {

    }

    private var mOrientation: Int = 0

    /**
     * Draws horizontal or vertical dividers onto the parent RecyclerView.
     *
     * @param canvas The {@link Canvas} onto which dividers will be drawn
     * @param parent The RecyclerView onto which dividers are being added
     * @param state The current RecyclerView.State of the RecyclerView
     */
    override fun onDraw(canvas: Canvas?, parent: RecyclerView?, state: RecyclerView.State?) {
        if (mOrientation == LinearLayoutManager.HORIZONTAL) {
            drawHorizontalDividers(canvas, parent);
        } else if (mOrientation == LinearLayoutManager.VERTICAL) {
            drawVerticalDividers(canvas, parent);
        }
    }

    /**
     * Determines the size and location of offsets between items in the parent
     * RecyclerView.
     *
     * @param outRect The {@link Rect} of offsets to be added around the child
     *                view
     * @param view The child view to be decorated with an offset
     * @param parent The RecyclerView onto which dividers are being added
     * @param state The current RecyclerView.State of the RecyclerView
     */
    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)

        if (parent?.getChildAdapterPosition(view) == 0) {
            return;
        }

        mOrientation = (parent?.getLayoutManager() as LinearLayoutManager).orientation
        if (mOrientation == LinearLayoutManager.HORIZONTAL) {
            outRect?.left = mDivider?.intrinsicWidth
        } else if (mOrientation == LinearLayoutManager.VERTICAL) {
            outRect?.top = mDivider?.intrinsicHeight
        }
    }

    /**
     * Adds dividers to a RecyclerView with a LinearLayoutManager or its
     * subclass oriented horizontally.
     *
     * @param canvas The {@link Canvas} onto which horizontal dividers will be
     *               drawn
     * @param parent The RecyclerView onto which horizontal dividers are being
     *               added
     */
    private fun drawHorizontalDividers(canvas: Canvas?, parent: RecyclerView?) {

        if (parent == null) {
            return
        }

        var parentTop = parent.paddingTop
        var parentBottom = parent.height - parent.paddingBottom

        var i = 0
        var childCount = parent.getChildCount()
        while (i < childCount - 1) {
            var child = parent.getChildAt(i)

            var params = child.layoutParams as RecyclerView.LayoutParams

            var parentLeft = child.right + params.rightMargin;
            var parentRight = parentLeft + (mDivider?.intrinsicWidth ?: 0)

            mDivider?.setBounds(parentLeft, parentTop, parentRight, parentBottom);
            mDivider?.draw(canvas)

            i++
        }
    }

    /**
     * Adds dividers to a RecyclerView with a LinearLayoutManager or its
     * subclass oriented vertically.
     *
     * @param canvas The {@link Canvas} onto which vertical dividers will be
     *               drawn
     * @param parent The RecyclerView onto which vertical dividers are being
     *               added
     */
    private fun drawVerticalDividers(canvas: Canvas?, parent: RecyclerView?) {

        if (parent == null) {
            return
        }

        var parentLeft = parent.paddingLeft;
        var parentRight = parent.width - parent.getPaddingRight();

        var i = 0
        var childCount = parent.childCount
        while (i < childCount - 1) {
            var child = parent.getChildAt(i);

            var params = child.layoutParams as RecyclerView.LayoutParams

            var parentTop = child.bottom + params.bottomMargin;
            var parentBottom = parentTop + (mDivider?.intrinsicHeight ?: 0);

            mDivider?.setBounds(parentLeft, parentTop, parentRight, parentBottom);
            mDivider?.draw(canvas);

            i++
        }
    }
}