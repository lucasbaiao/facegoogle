package br.com.babuka.facegoogle.view


interface BasePresenter<T> {

    fun start()

    var view: T
}