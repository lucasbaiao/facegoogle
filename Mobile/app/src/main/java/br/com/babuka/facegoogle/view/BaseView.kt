package br.com.babuka.facegoogle.view

interface BaseView<out T : BasePresenter<*>> {

    val presenter: T

}
