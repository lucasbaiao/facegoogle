package br.com.babuka.facegoogle.view.categorias

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import br.com.babuka.domain.entities.Category
import br.com.babuka.facegoogle.R
import br.com.babuka.facegoogle.components.widgets.DividerItemDecoration
import br.com.babuka.facegoogle.components.widgets.ScrollChildSwipeRefreshLayout
import br.com.babuka.facegoogle.util.showSnackBar
import org.koin.android.ext.android.inject

class CategoriesFragment : Fragment(), CategoriesContract.View, SearchView.OnQueryTextListener {

    private lateinit var noPostsView: View
    private lateinit var noPostIcon: ImageView
    private lateinit var noPostMainView: TextView
    private lateinit var postsView: View

    private var itemListener: CategoriesAdapter.CategoryItemListener = object : CategoriesAdapter.CategoryItemListener {
        override fun onItemClick(clickedItem: Category, view: View) {
            presenter.showCategoryChildren(clickedItem)
        }
    }

    private var listAdapter = CategoriesAdapter(ArrayList(0), itemListener)

    override val presenter by inject<CategoriesContract.Presenter>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_empty_list, container, false)
        with(root) {
            val listView = findViewById<RecyclerView>(R.id.recycler_view).apply {
                setHasFixedSize(true)
                adapter = listAdapter
                addItemDecoration(DividerItemDecoration(context))
            }

            // Set up progress indicator
            findViewById<ScrollChildSwipeRefreshLayout>(R.id.refresh_layout).apply {
                setColorSchemeColors(
                        ContextCompat.getColor(activity!!, R.color.colorPrimary),
                        ContextCompat.getColor(activity!!, R.color.colorAccent),
                        ContextCompat.getColor(activity!!, R.color.colorPrimaryDark)
                )
                // Set the scrolling view in the custom SwipeRefreshLayout.
                scrollUpChild = listView
                setOnRefreshListener { presenter.loadCategories(false) }
            }

            postsView = findViewById(R.id.recycler_view)
            noPostsView = findViewById(R.id.noDataContainer)
            noPostIcon = findViewById(R.id.noDataIcon)
            noPostMainView = findViewById(R.id.noDataText)
        }
        setHasOptionsMenu(true)
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search, menu)
        val item = menu?.findItem(R.id.action_search)
        (item?.actionView as SearchView?)?.setOnQueryTextListener(this)
        item?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {

            override fun onMenuItemActionExpand(item: MenuItem?): Boolean = true

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                listAdapter.filter("")
                postsView.findViewById<RecyclerView>(R.id.recycler_view).scrollToPosition(0)
                return true
            }
        })
    }

    /*
    * SearchView.OnQueryTextListener
    */
    override fun onQueryTextSubmit(query: String?): Boolean = false

    override fun onQueryTextChange(newText: String?): Boolean {
        listAdapter.filter(newText ?: "")
        postsView.findViewById<RecyclerView>(R.id.recycler_view).scrollToPosition(0)

        return true
    }

    override fun doBack(): Boolean {
        return (!presenter.showPreviousLevel())
    }

    override fun setLoadingIndicator(isVisible: Boolean) {
        val root = view ?: return
        with(root.findViewById<SwipeRefreshLayout>(R.id.refresh_layout)) {
            post { isRefreshing = isVisible }
        }
    }

    override fun showLoadingError() {
        showMessage(getString(R.string.loading_categories_error))
    }

    override fun showData(data: List<Category>) {
        listAdapter.clear()
        data.forEach { item -> listAdapter.addItem(item) }
        postsView.visibility = View.VISIBLE
        noPostsView.visibility = View.GONE
    }

    override fun showNoData() {
        showNoPostsViews(resources.getString(R.string.no_data), R.drawable.ic_assignment_turned_in_24dp, false)
    }

    override fun setTitle(title: String) {
        this.activity?.title = title
    }

    override var isActive: Boolean = false
        get() = isAdded

    override fun onResume() {
        super.onResume()
        presenter.view = this
        presenter.start()
    }

    private fun showMessage(message: String) {
        postsView.showSnackBar(message, Snackbar.LENGTH_LONG)
    }

    private fun showNoPostsViews(mainText: String, iconRes: Int, showAddView: Boolean) {
        postsView.visibility = View.GONE
        noPostsView.visibility = View.VISIBLE

        noPostMainView.text = mainText
        noPostIcon.setImageResource(iconRes)
    }
}