package br.com.babuka.facegoogle.view.categorias

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.babuka.domain.entities.Category
import br.com.babuka.facegoogle.R
import br.com.babuka.facegoogle.components.view.adapter.FilterableBaseAdapter

class CategoriesAdapter constructor(dataSet: ArrayList<Category>, itemListener: CategoryItemListener)
    : FilterableBaseAdapter<Category, CategoriesAdapter.ViewHolder>(dataSet) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowView = LayoutInflater.from(parent?.context).inflate(R.layout.simple_image_list_item, parent, false)
        return ViewHolder(itemView = rowView, itemListener = _itemListener)
    }

    private var _itemListener: ViewHolderItemListener


    override fun bindView(item: Category, holder: ViewHolder) {
        holder.bindView(item)
    }

    interface CategoryItemListener {
        fun onItemClick(clickedItem: Category, view: View)
    }

    internal interface ViewHolderItemListener {
        fun onItemClick(position: Int, view: View)
    }

    class ViewHolder : FilterableBaseAdapter.ViewHolder {
        internal var text: TextView?
        private var image: ImageView?


        internal constructor(itemView: View, itemListener: ViewHolderItemListener) : super(itemView) {
            this.text = itemView.findViewById<TextView>(R.id.text)
            this.image = itemView.findViewById<ImageView>(R.id.image)
            itemView.setOnClickListener {
                itemListener.onItemClick(adapterPosition, itemView)
            }
        }

        fun bindView(item: Category) {
            text?.text = item.title
        }
    }

    init {
        this._itemListener = object : ViewHolderItemListener {
            override fun onItemClick(position: Int, view: View) {
                itemListener.onItemClick(publishResults[position], view)
            }
        }
    }
}