package br.com.babuka.facegoogle.view.post

import android.app.Activity
import br.com.babuka.domain.entities.Posts
import br.com.babuka.facegoogle.view.BasePresenter
import br.com.babuka.facegoogle.view.BaseView

/**
 * This specifies the contract between the view and the presenter.
 */
interface PostsContract {

    interface View : BaseView<Presenter> {

        var isActive: Boolean

        fun setLoadingIndicator(active: Boolean)

        fun showAddPost(post: Posts)

        fun showFilteringPopUpMenu()

        fun showLoadingPostsError()

        fun showNoPosts()

        fun showAllFilterLabel()

        fun showPosts(posts: List<Posts>)

        fun showPostDetails(clickedPost: Posts, view: android.view.View)
    }

    interface Presenter : BasePresenter<View> {
        val liveQuery: com.couchbase.lite.LiveQuery

        fun loadPosts()

        fun openPostDetails(clickedPost: Posts, view: android.view.View)
    }
}