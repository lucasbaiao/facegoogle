package br.com.babuka.facegoogle.view.post

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.babuka.domain.entities.Posts
import br.com.babuka.facegoogle.R
import com.bumptech.glide.Glide
import com.couchbase.lite.LiveQuery
import com.couchbase.lite.QueryRow
import com.google.common.collect.Lists


class PostAdapter(posts: List<Posts>, private val itemListener: PostItemListener) : RecyclerView.Adapter<PostAdapter.PostAdapterViewHolder>() {

    private var liveQuery: LiveQuery? = null

    var posts: List<Posts> = posts
        set(posts) {
            field = posts
            notifyDataSetChanged()
        }

    fun add(post: Posts) {
        val newList = Lists.newArrayList<Posts>()
        newList.addAll(posts)
        newList.add(post)
        posts = newList
    }

    fun addChangeListener(query: LiveQuery) {
        this.liveQuery = query
        this.liveQuery!!.addChangeListener({ event: LiveQuery.ChangeEvent? ->

            if (event != null) {
                val result = Lists.newArrayList<Posts>()

                val rows = event.rows
                var row: QueryRow? = rows.next()
                while (row != null) {
                    val value = row.asJSONDictionary()["value"]
                    val instance = Posts.PostFactory().create(value as Map<String, Any>).withId(row.documentId).instance

                    result.add(instance)

                    row = rows.next()
                }
                itemListener.onReload(result)
            }

        })
        this.liveQuery!!.start()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostAdapterViewHolder {
        val rowView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_posts_item, parent, false)
        return PostAdapterViewHolder(rowView)
    }

    override fun getItemId(position: Int): Long {
        return posts[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    private val imageDictionary: MutableMap<String, Bitmap> = mutableMapOf()

    override fun onBindViewHolder(holder: PostAdapterViewHolder, position: Int) {
        val post = posts[position]
        if (imageDictionary[post.documentId!!] == null && post.image != null) {
            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.RGB_565
            imageDictionary[post.documentId!!] = BitmapFactory.decodeStream(post.image, null, options)
        }

        Glide.with(holder.itemView.context).load(imageDictionary[post.documentId!!]).into(holder.image)
        holder.title.text = post.title
        holder.views.text = kotlin.String.format("%d %s", post.views, holder.itemView.context.getText(br.com.babuka.facegoogle.R.string.views))
        holder.shareButton.setOnClickListener { itemListener.onShareClicked(post, holder.shareButton) }
        holder.itemView.setOnClickListener { itemListener.onPostClick(post, holder.itemView) }
    }

    interface PostItemListener {
        fun onReload(post: List<Posts>)
        fun onShareClicked(post: Posts, view: View)
        fun onPostClick(clickedPost: Posts, view: View)
    }

    class PostAdapterViewHolder : RecyclerView.ViewHolder {
        var image: ImageView
        var title: TextView
        var views: TextView
        var shareButton: AppCompatImageButton

        internal constructor(itemView: View) : super(itemView) {
            image = itemView.findViewById(R.id.image)
            title = itemView.findViewById(R.id.title)
            views = itemView.findViewById(R.id.views)
            shareButton = itemView.findViewById(R.id.post_share)
        }
    }
}