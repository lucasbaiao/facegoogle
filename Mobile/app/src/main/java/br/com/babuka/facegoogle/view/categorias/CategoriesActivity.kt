package br.com.babuka.facegoogle.view.categorias

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import br.com.babuka.facegoogle.R
import br.com.babuka.facegoogle.view.BaseActivity
import org.koin.android.ext.android.inject

class CategoriesActivity : BaseActivity() {

    private val categoriesFragment: CategoriesFragment by inject()

    override fun onBackPressed() {
        if(!categoriesFragment.doBack())
            super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        setupContent()
    }

    private fun setupContent() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.contentFrame, categoriesFragment)
                .commit()
    }
}