package br.com.babuka.facegoogle.view.home

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import br.com.babuka.facegoogle.R
import br.com.babuka.facegoogle.view.BaseActivity
import br.com.babuka.facegoogle.view.categorias.CategoriesActivity
import br.com.babuka.facegoogle.view.categorias.CategoriesFragment
import br.com.babuka.facegoogle.view.post.PostsFragment
import org.koin.android.ext.android.inject

class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val postsFragment: PostsFragment by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        setupDrawer(toolbar)
        displayView(R.id.nav_feed)
    }

    private fun setupDrawer(toolbar: Toolbar) {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        item.isChecked = true
        title = item.title

        displayView(item.itemId)

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun displayView(itemId: Int) {
        var fragment: Fragment? = null
        when (itemId) {
            R.id.nav_feed -> fragment = postsFragment
            R.id.nav_places -> {
            }
            R.id.nav_categories -> {
                super.navigate(CategoriesActivity::class)
            }
            R.id.nav_config -> {
            }
            R.id.nav_account -> {
            }
            R.id.nav_publish -> {
            }
        }

        if (fragment != null) {
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction().replace(R.id.contentFrame, fragment).commit()
        }
    }
}