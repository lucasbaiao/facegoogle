package br.com.babuka.facegoogle.view

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import br.com.babuka.facegoogle.R
import kotlin.reflect.KClass

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    enum class AnimationActivity {
        None, BottomToTop
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return true
    }

    protected fun <T : AppCompatActivity> navigate(kClass: KClass<T>) {
        navigate(kClass, AnimationActivity.None)
    }

    protected fun <T : AppCompatActivity> navigate(kClass: KClass<T>, anim: AnimationActivity) {
        val intent = Intent(this, kClass.java)
        startActivity(intent)
        when (anim) {
            AnimationActivity.BottomToTop -> {
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
            else -> {
            }
        }
    }
}
