package br.com.babuka.facegoogle

import android.app.Application
import android.content.pm.ApplicationInfo
import br.com.babuka.couchbase.services.CouchbaseCommunicationService
import br.com.babuka.couchbase.services.configuration.ChannelConfiguration
import br.com.babuka.facegoogle.di.todoAppModules
import com.testfairy.TestFairy
import org.koin.Koin
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        TestFairy.begin(this, "a22f6da1418726fe160849da958d64fa452ad538");

        val isDebug = (0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE)
        if (isDebug) {
            Koin.logger = AndroidLogger()
        }

        startKoin(this, todoAppModules)
        startCouchbase()
    }

    private fun startCouchbase() {
        val communication = CouchbaseCommunicationService(this)
        communication.setupCommunication()
        communication.addChannelToPushAndPull(ChannelConfiguration.Posts)
        communication.addChannelToPushAndPull(ChannelConfiguration.Empresa)
    }
}