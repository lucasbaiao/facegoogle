package br.com.babuka.facegoogle.view.categorias

import br.com.babuka.domain.entities.Category
import br.com.babuka.domain.interfaces.repositories.CategoriaDataSource
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent


class CategoriesPresenter(private val categoryParent: String, private val categoryRepository: CategoriaDataSource)
    : CategoriesContract.Presenter, KoinComponent {

    override lateinit var view: CategoriesContract.View
    private var firstLoad = true
    private var _category: Category? = null

    override fun showPreviousLevel(): Boolean {
        val temp = Category.Factory.fromParent(_category).instance;
        loadCategories(_category?.group ?: "", true, true)
        return (temp?.level ?: 0) < 1
    }

    override fun start() {
        loadCategories(false)
    }

    override fun showCategoryChildren(clickedItem: Category) {
        loadCategories(clickedItem.documentId!!, true, true)
    }

    override fun loadCategories(forceUpdate: Boolean) {
        loadCategories(categoryParent, forceUpdate || firstLoad, true)
        firstLoad = false
    }

    private fun setViewTitle(id: String) {
        Observable.create(getCategoryIdObservable(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> onCategoryLoaded(result) },
                        { error -> _category = null }
                )
    }

    private fun getCategoryIdObservable(id: String): ObservableOnSubscribe<Category> {
        return ObservableOnSubscribe { e: ObservableEmitter<Category> ->
            e.onNext(categoryRepository.GetById(id))
            e.onComplete()
        }
    }

    private fun getCategoryAllObservable(): ObservableOnSubscribe<List<Category>> {
        return ObservableOnSubscribe { e: ObservableEmitter<List<Category>> ->
            e.onNext(categoryRepository.getAll())
            e.onComplete()
        }
    }

    private fun onCategoryLoaded(category: Category) {
        _category = category
        view.setTitle(category.title)
    }

    private fun loadCategories(parentId: String, forceUpdate: Boolean, showLoadingUI: Boolean) {

        if (showLoadingUI) {
            view.setLoadingIndicator(true)
        }

        this.setViewTitle(parentId)

        Observable.create(getCategoryAllObservable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> onCategoriesDataLoaded(result, showLoadingUI) },
                        { error -> onCategoryDataNotAvailable() }
                )
    }

    private fun onCategoriesDataLoaded(data: List<Category>, showLoadingUI: Boolean) {

        val postsToShow = ArrayList<Category>()
        postsToShow += data

        if (!view.isActive) {
            return
        }
        if (showLoadingUI) {
            view.setLoadingIndicator(false)
        }

        if (data.isEmpty()) {
            view.showNoData()
        } else {
            view.showData(data)
        }
    }

    fun onCategoryDataNotAvailable() {
        if (!view.isActive) {
            return
        }
        view.showLoadingError()
    }
}
