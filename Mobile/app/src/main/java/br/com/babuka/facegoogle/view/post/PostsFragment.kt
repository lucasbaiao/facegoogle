package br.com.babuka.facegoogle.view.post

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import br.com.babuka.domain.entities.Posts
import br.com.babuka.facegoogle.R
import br.com.babuka.facegoogle.components.widgets.ScrollChildSwipeRefreshLayout
import br.com.babuka.facegoogle.util.showSnackBar
import br.com.babuka.facegoogle.view.post.detail.PostDetailActivity
import org.koin.android.ext.android.inject


class PostsFragment : Fragment(), PostsContract.View {

    override val presenter by inject<PostsContract.Presenter>()

    private lateinit var noPostsView: View
    private lateinit var noPostIcon: ImageView
    private lateinit var noPostMainView: TextView
    private lateinit var postsView: LinearLayout
    private lateinit var filteringLabelView: TextView

    override var isActive: Boolean = false
        get() = isAdded

    /**
     * Listener for clicks on dataSet in the ListView.
     */
    private var itemListener: PostAdapter.PostItemListener = object : PostAdapter.PostItemListener {

        override fun onReload(post: List<Posts>) {
            activity?.runOnUiThread({
                listAdapter.posts = post
            })
        }

        override fun onPostClick(clickedPost: Posts, view: View) {
            presenter.openPostDetails(clickedPost, view)
        }

        override fun onShareClicked(post: Posts, view: View) {
            sharePost(post)
        }
    }

    private fun sharePost(post: Posts) {
        Log.d("Facebook api", "onShareClicked" + post.toString())
    }

    private val listAdapter = PostAdapter(ArrayList(0), itemListener).apply {
        addChangeListener(presenter.liveQuery)
        setHasStableIds(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_posts, container, false)

        // Set up dataSet view
        with(root) {
            val listView = findViewById<RecyclerView>(R.id.posts_list).apply {
                adapter = listAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                setItemViewCacheSize(20)
                isDrawingCacheEnabled = true
                drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
            }


            findViewById<ScrollChildSwipeRefreshLayout>(R.id.refresh_layout).apply {
                setColorSchemeColors(
                        ContextCompat.getColor(activity!!, R.color.colorPrimary),
                        ContextCompat.getColor(activity!!, R.color.colorAccent),
                        ContextCompat.getColor(activity!!, R.color.colorPrimaryDark)
                )
                // Set the scrolling view in the custom SwipeRefreshLayout.
                scrollUpChild = listView
                setOnRefreshListener { presenter.loadPosts() }
            }

            filteringLabelView = findViewById(R.id.filteringLabel)
            postsView = findViewById(R.id.postsLL)

            // Set up  no dataSet view
            noPostsView = findViewById(R.id.noPosts)
            noPostIcon = findViewById(R.id.noPostsIcon)
            noPostMainView = findViewById(R.id.noPostsMain)
        }

        setHasOptionsMenu(true)
        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_filter -> showFilteringPopUpMenu()
            R.id.menu_refresh -> presenter.loadPosts()
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.posts, menu)
    }

    override fun onResume() {
        super.onResume()
        presenter.view = this
        presenter.start()
    }

    override fun showLoadingPostsError() {
        showMessage(getString(R.string.loading_posts_error))
    }

    override fun showNoPosts() {
        showNoPostsViews(resources.getString(R.string.no_posts_all), R.drawable.ic_assignment_turned_in_24dp, false)
    }

    override fun showAllFilterLabel() {
        filteringLabelView.text = resources.getString(R.string.label_all)
    }

    override fun showPosts(posts: List<Posts>) {
        listAdapter.posts = posts
        postsView.visibility = View.VISIBLE
        noPostsView.visibility = View.GONE
    }

    override fun setLoadingIndicator(active: Boolean) {
        val root = view ?: return
        with(root.findViewById<SwipeRefreshLayout>(R.id.refresh_layout)) {
            // Make sure setRefreshing() is called after the layout is done with everything else.
            post { isRefreshing = active }
        }
    }

    override fun showAddPost(post: Posts) {
        listAdapter.add(post)
    }

    override fun showFilteringPopUpMenu() {
        PopupMenu(context!!, activity!!.findViewById(R.id.menu_filter)).apply {
            menuInflater.inflate(R.menu.filter_posts, menu)
            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                //R.id.active -> presenter.currentFiltering = TasksFilterType.ACTIVE_TASKS
                //R.id.completed -> presenter.currentFiltering = TasksFilterType.COMPLETED_TASKS
                //else -> presenter.currentFiltering = TasksFilterType.ALL_TASKS
                }
                presenter.loadPosts()
                true
            }
            show()
        }
    }

    override fun showPostDetails(clickedPost: Posts, view: View) {
        val imageView = view.findViewById<ImageView>(R.id.image)
        val intent = Intent(context, PostDetailActivity::class.java)

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, imageView, ViewCompat.getTransitionName(imageView))
        startActivity(intent, options.toBundle())
    }

    private fun showMessage(message: String) {
        view?.showSnackBar(message, Snackbar.LENGTH_LONG)
    }

    private fun showNoPostsViews(mainText: String, iconRes: Int, showAddView: Boolean) {
        postsView.visibility = View.GONE
        noPostsView.visibility = View.VISIBLE

        noPostMainView.text = mainText
        noPostIcon.setImageResource(iconRes)
        noPostMainView.visibility = if (showAddView) View.VISIBLE else View.GONE
    }
}




