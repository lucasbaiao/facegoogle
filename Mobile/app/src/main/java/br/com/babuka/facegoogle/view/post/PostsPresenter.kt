package br.com.babuka.facegoogle.view.post

import br.com.babuka.couchbase.repository.CouchbasePostsRepository
import br.com.babuka.domain.entities.Posts
import br.com.babuka.domain.interfaces.repositories.PostsDataSource
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent


class PostsPresenter(private val postsRepository: PostsDataSource)
    : PostsContract.Presenter, KoinComponent {

    override lateinit var view: PostsContract.View
    private var firstLoad = true

    override val liveQuery: com.couchbase.lite.LiveQuery
        get() {
            val base = (postsRepository as CouchbasePostsRepository)
            return base.liveQuery
        }

    override fun start() {
        loadPosts()
    }

    override fun loadPosts() {
        loadPosts(true)
        firstLoad = false
    }

    override fun openPostDetails(clickedPost: Posts, view: android.view.View) {
        this.view.showPostDetails(clickedPost, view)
    }

    private fun loadPosts(showLoadingUI: Boolean) {
        if (showLoadingUI) {
            view.setLoadingIndicator(true)
        }

        Observable.create(getPostObservable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> onPostsLoaded(result, showLoadingUI) },
                        { error -> onPostsLoadError(error) }
                )
    }

    private fun getPostObservable(): ObservableOnSubscribe<List<Posts>> {
        return ObservableOnSubscribe { e: ObservableEmitter<List<Posts>> ->
            e.onNext(postsRepository.getAll())
            e.onComplete()
        }
    }

    private fun onPostsLoaded(posts: List<Posts>, showLoadingUI: Boolean) {
        val postsToShow = ArrayList<Posts>()
        postsToShow += posts

        if (!view.isActive) {
            return
        }
        if (showLoadingUI) {
            view.setLoadingIndicator(false)
        }

        if (posts.isEmpty()) {
            view.showNoPosts()
        } else {
            view.showPosts(posts)
        }
    }

    private fun onPostsLoadError(error: Throwable) {
        if (!view.isActive) {
            return
        }
        view.showLoadingPostsError()
    }
}
