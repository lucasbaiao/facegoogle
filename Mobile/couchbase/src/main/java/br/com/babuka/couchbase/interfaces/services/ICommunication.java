package br.com.babuka.couchbase.interfaces.services;

import java.net.MalformedURLException;
import java.util.List;

import br.com.babuka.couchbase.services.configuration.ChannelConfiguration;

public interface ICommunication {

    void setupCommunication() throws MalformedURLException;
    void dispose();
    void AddChannelsToPull(List<ChannelConfiguration> channels);
    void AddChannelsToPush(List<ChannelConfiguration> channels);
    void AddChannelsToPushAndPull(List<ChannelConfiguration> channels);
    void AddChannelToPull(ChannelConfiguration channel);
    void AddChannelToPush(ChannelConfiguration channel);
    void addChannelToPushAndPull(ChannelConfiguration channel);
    void RemoveChannelsToPushAndPull(List<ChannelConfiguration> channels);
    void RemoveChannelToPull(ChannelConfiguration channel);
    void RemoveChannelToPush(ChannelConfiguration channel);
    void RemoveChannelToPushAndPull(ChannelConfiguration channel);
    void StartPull();
    void StartPush();
    void StartSync();
    void StopPull();
    void StopPush();
    void StopSync();
    void restart() throws MalformedURLException;
    String GetPushStatus();
    String GetPullStatus();
    String GetPullerProgress();
    List<String> GetPullChannels();
    List<String> GetPushChannels();

}
