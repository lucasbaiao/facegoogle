package br.com.babuka.couchbase.services.configuration;

public class ChannelConfiguration {

    public static ChannelConfiguration EmpresaChannel = new ChannelConfiguration("EmpresaToken");
    public static ChannelConfiguration Empresa = new ChannelConfiguration("Empresa");
    public static ChannelConfiguration Posts = new ChannelConfiguration("Posts");
    public static ChannelConfiguration Categorias = new ChannelConfiguration("Categorias");

    private String _name;
    private String nameGlobal;
    private String nameLocal;

    private String GetNomeLocal() {
        String nome = _name;
        if (_name == "EmpresaToken") {
            nome = "EmpresaChannel";
            nameGlobal = nome;
        } else {
            String empresaChannel = "EmpresaChannel";
            nome = String.format("%s_%s", _name, empresaChannel);
        }
        return nome;
    }

    public String getNameGlobal() {
        return nameGlobal;
    }

    public String getNameLocal() {
        return GetNomeLocal();
    }

    public ChannelConfiguration(String name) {
        if (name != "EmpresaToken") {
            nameGlobal = name;
        } else {
            nameGlobal = "";
        }

        _name = name;
    }

    @Override
    public String toString() {
        return nameGlobal;
    }
}