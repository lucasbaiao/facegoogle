package br.com.babuka.couchbase.repository.configuration;

import android.content.Context;
import android.util.Log;

import com.couchbase.lite.DatabaseOptions;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.store.SQLiteStore;

public class CouchbaseDatabaseInfo {

    private static String DATABASENAME = "database";
    private  com.couchbase.lite.Database Database;
    private  com.couchbase.lite.Manager Manager;
    private static CouchbaseDatabaseInfo Instance;

    private CouchbaseDatabaseInfo(Context context) {
        setupDataBase(context);
    }

    public static com.couchbase.lite.Database getDatabase(Context context) {
        if (Instance == null)
            Instance = new CouchbaseDatabaseInfo(context);
        return Instance.Database;
    }

    public static com.couchbase.lite.Manager getManager(Context context) {
        if (Instance == null)
            Instance = new CouchbaseDatabaseInfo(context);
        return Instance.Manager;
    }

    private void setupDataBase(Context context) {
        try {
            if (Manager == null) {
                Manager = new com.couchbase.lite.Manager(new AndroidContext(context), com.couchbase.lite.Manager.DEFAULT_OPTIONS);
                com.couchbase.lite.store.SQLiteStore a = new SQLiteStore(Manager.getDirectory().getAbsolutePath(), Manager, null);

                DatabaseOptions options = new DatabaseOptions();
                options.setCreate(true);
                options.setStorageType(com.couchbase.lite.Manager.SQLITE_STORAGE);
                Database = Manager.openDatabase(DATABASENAME, options);
            }
        } catch (Exception e) {
            Log.e("CouchbaseDatabaseInfo", e.getMessage(), e);
        }
    }
}
