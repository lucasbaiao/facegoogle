package br.com.babuka.couchbase.repository;

import android.content.Context;
import android.util.Log;

import com.annimon.stream.Stream;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.View;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import br.com.babuka.couchbase.repository.configuration.CouchbaseDatabaseInfo;
import br.com.babuka.couchbase.services.configuration.ChannelConfiguration;
import br.com.babuka.domain.interfaces.entitites.IEntity;
import br.com.babuka.domain.interfaces.entitites.IEntityFactory;
import br.com.babuka.domain.interfaces.repositories.IBaseRepository;

public abstract class CouchbaseBaseRepository<T extends IEntity<T, K>, K extends IEntityFactory<T, K>> implements IBaseRepository<T, K> {

    private final Class<K> tClass;
    private String _entityType;
    protected com.couchbase.lite.Database database;
    protected List<ChannelConfiguration> _channels;

    protected abstract String ViewName();

    protected abstract String ViewVersion();

    CouchbaseBaseRepository(ChannelConfiguration entityType, Context context, Class<K> tClass) {
        this._entityType = entityType.getNameGlobal();
        this.tClass = tClass;
        this._channels = new ArrayList<>();
        this.database = CouchbaseDatabaseInfo.getDatabase(context);
    }

    List<T> GetAllCollection() throws CouchbaseLiteException {
        List<T> result = new ArrayList<>();
        QueryEnumerator queryRows = GetAllView().createQuery().run();
        while (queryRows.hasNext()) {
            try {
                QueryRow row = queryRows.next();
                Document doc = database.getDocument(row.getDocumentId());
                if (doc != null && !doc.isDeleted()) {
                    Object value = row.asJSONDictionary().get("value");

                    K factory = (K) Class.forName(this.tClass.getName()).newInstance();
                    factory = factory.create((Map<String, Object>) value);
                    T instance = factory.withId(row.getDocumentId()).getInstance();
                    result.add(instance);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(getClass().getSimpleName(), e.getMessage(), e);
            }
        }
        return result;
    }

    protected View GetAllView() {
        View view = database.getView(ViewName());
        ///view.updateIndex();
        if (view.getMap() == null) {
            view.setMap((document, emitter) -> {
                if (IsDocACurrentEntity(document)) {
                    emitter.emit(_entityType, GetDocumentValue(document));
                }
            }, ViewVersion());
        }
        return view;
    }

    private boolean IsDocACurrentEntity(Map<String, Object> doc) {
        if (doc.containsKey("Type")) {
            return doc.get("Type").toString().equals(_entityType);
        }
        return false;
    }

    private Document CreateDocument(IEntity<T, K> obj, List<String> channels) {
        Map<String, Object> documentTemplate = GetDocumentTemplate(obj, channels);
        Document doc = database.createDocument();
        try {
            doc.putProperties(documentTemplate);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        return doc;
    }

    private Map<String, Object> GetDocumentTemplate(IEntity<T, K> obj, List<String> channels) {
        return new Hashtable<String, Object>() {
            {
                put("Type", _entityType);
                put("Content", obj);
                put("channels", channels);
            }
        };
    }

    public String Insert(T entity) {
        return Insert(entity, new ChannelConfiguration[0]);
    }

    public String Insert(T entity, ChannelConfiguration... channels) {
        List<String> channelsNames = new ArrayList<String>();
        List<ChannelConfiguration> channelsList = (List<ChannelConfiguration>) Stream.of(channels)
                .filter(channelConfigurations -> channelConfigurations.getNameGlobal() != "");

        channelsList.addAll(Arrays.asList(channels));
        channelsList.add(ChannelConfiguration.EmpresaChannel);
        for (ChannelConfiguration c : channelsList) {
            if (!c.getNameGlobal().equals(""))
                channelsNames.add(c.getNameGlobal());

            if (!c.getNameGlobal().equals(c.getNameLocal())) {
                channelsNames.add(c.getNameLocal());
            }
        }

        return CreateDocument(entity, channelsNames).getId();
    }

    public void Delete(String documentId) {
        try {
            database.getDocument(documentId).delete();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public void Update(T entity) {
        Document doc = database.getDocument(entity.getDocumentId());
        try {
            doc.update(newRevision ->
            {
                Map<String, Object> props = newRevision.getProperties();
                props.put("Content", entity);
                return true;
            });
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    protected String GetDocumentType(HashMap<String, Object> doc) {
        if (doc.keySet().contains("Type")) {
            return doc.get("Type").toString();
        } else {
            throw new IllegalArgumentException("Entidade fora do padrão");
        }
    }

    protected Object GetDocumentValue(Map<String, Object> doc) {
        return doc.get("Content");
    }

    public T GetById(String documentId) {
        Document doc = database.getDocument(documentId);
        if ((doc.getProperties().get("Content")) != null) {
            return (T) doc.getProperties().get("Content");
        } else {
            try {
                String data = (GetDocumentValue(doc.getUserProperties()).toString());

                ObjectMapper mapper = new ObjectMapper();
                K factory = mapper.readValue(data, this.tClass);
                return factory.withId(documentId).getInstance();
            } catch (IOException e) {
                return null;
            }
        }
    }
}