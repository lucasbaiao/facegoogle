package br.com.babuka.couchbase.repository;

import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.babuka.couchbase.services.configuration.ChannelConfiguration;
import br.com.babuka.domain.entities.Posts;
import br.com.babuka.domain.interfaces.repositories.PostsDataSource;


public class CouchbasePostsRepository extends CouchbaseBaseRepository<Posts, Posts.PostFactory> implements PostsDataSource {
    private final String Imagem = "posts.jpg";
    private final String ContentType = "image/jpg";

    public CouchbasePostsRepository(Context context) {
        super(ChannelConfiguration.Posts, context, Posts.PostFactory.class);
        _channels.add(ChannelConfiguration.Posts);
    }

    public CouchbasePostsRepository(ChannelConfiguration entityType, Context context) {
        super(entityType, context, Posts.PostFactory.class);
    }

    @Override
    protected String ViewName() {
        return "PostsView";
    }

    @Override
    protected String ViewVersion() {
        return "5";
    }

    public com.couchbase.lite.LiveQuery getLiveQuery() {
        return super.GetAllView().createQuery().toLiveQuery();
    }

    public InputStream GetImagem(Posts post) {
        return GetImagem(post.getDocumentId());
    }

    private InputStream GetImagem(String DocumentId) {
        Document doc = database.getDocument(DocumentId);
        InputStream content = null;
        try {
            if (doc.getCurrentRevision().getAttachment(Imagem) != null && doc.getCurrentRevision().getAttachment(Imagem).getContent().available() != 0) {
                content = doc.getCurrentRevision().getAttachment(Imagem).getContent();
            }
        } catch (NullPointerException  e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
        return content;
    }

    @NotNull
    @Override
    public List<Posts> getAll() {
        try {
            List<Posts> result = new ArrayList<>();
            List<Posts> posts = GetAllCollection();
            for (Posts post : posts) {
                result.add(Posts.Factory.from(post).withImagem(GetImagem(post)).getInstance());
            }
            return result;
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}

