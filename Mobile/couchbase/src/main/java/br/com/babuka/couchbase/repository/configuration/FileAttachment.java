package br.com.babuka.couchbase.repository.configuration;

import java.util.stream.Stream;

public class FileAttachment {

    private static String CONTENT_TYPE_JPG = "image/jpg";

    public static CouchbaseAttachmentFactory Factory = new CouchbaseAttachmentFactory();
    private String Name;
    private Stream Content;
    private String ContentType;

    private FileAttachment(String name, Stream content, String contentType) {
        Name = name;
        Content = content;
        ContentType = contentType;
    }

    public static class CouchbaseAttachmentFactory {
        public FileAttachment Create(String name, Stream content, String contentType) {
            return new FileAttachment(name, content, contentType);
        }

        public FileAttachment CreateJpg(String name, Stream content) {
            return new FileAttachment(name, content, CONTENT_TYPE_JPG);
        }
    }
}
