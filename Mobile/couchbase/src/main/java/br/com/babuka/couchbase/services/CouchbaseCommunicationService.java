package br.com.babuka.couchbase.services;

import android.content.Context;

import com.annimon.stream.Stream;
import com.couchbase.lite.replicator.Replication;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.babuka.couchbase.interfaces.services.ICommunication;
import br.com.babuka.couchbase.repository.configuration.CouchbaseDatabaseInfo;
import br.com.babuka.couchbase.services.configuration.ChannelConfiguration;

public class CouchbaseCommunicationService implements ICommunication {

    private double LastProgress;
    private Date LastCheckTime;
    private URL _syncGatewayAddress;
    private Replication _pull;
    private Replication _push;
    private List<String> _pullChannels;
    private List<String> _pushChannels;
    private Context context;

    public CouchbaseCommunicationService(Context context) {
        this.context = context;
        _pullChannels = new ArrayList<>();
        _pushChannels = new ArrayList<>();
    }

    private void setupSync() throws MalformedURLException {
        _syncGatewayAddress = new URL("http://10.0.2.2:4984/db");
        _pull = CouchbaseDatabaseInfo.getDatabase(context).createPullReplication(_syncGatewayAddress);
        _push = CouchbaseDatabaseInfo.getDatabase(context).createPushReplication(_syncGatewayAddress);
        _pull.setChannels(_pullChannels);
        _push.setChannels(_pushChannels);
        _push.setContinuous(true);
        _pull.setContinuous(true);
    }

    @Override
    public void setupCommunication() throws MalformedURLException {
        setupSync();
        restart();
    }

    @Override
    public void dispose() {
        CouchbaseDatabaseInfo.getDatabase(context).close();
        CouchbaseDatabaseInfo.getManager(context).close();
    }

    @Override
    public void AddChannelsToPull(List<ChannelConfiguration> channels) {
        for (ChannelConfiguration channelConfiguration : channels) {
            AddChannelToPull(channelConfiguration);
        }
    }

    @Override
    public void AddChannelsToPush(List<ChannelConfiguration> channels) {
        for (ChannelConfiguration channelConfiguration : channels) {
            AddChannelToPush(channelConfiguration);
        }
    }

    @Override
    public void AddChannelsToPushAndPull(List<ChannelConfiguration> channels) {
        AddChannelsToPull(channels);
        AddChannelsToPush(channels);
    }

    @Override
    public void AddChannelToPull(ChannelConfiguration channel) {
        AddChannelToReplication(_pull, channel.getNameLocal(), _pullChannels);
        if (channel.getNameLocal() != channel.getNameGlobal()) {
            AddChannelToReplication(_pull, channel.getNameGlobal(), _pullChannels);
        }
    }

    @Override
    public void AddChannelToPush(ChannelConfiguration channel) {
        AddChannelToReplication(_push, channel.getNameLocal(), _pushChannels);
    }

    @Override
    public void addChannelToPushAndPull(ChannelConfiguration channel) {
        AddChannelToPull(channel);
        AddChannelToPush(channel);
    }

    @Override
    public void RemoveChannelsToPushAndPull(List<ChannelConfiguration> channels) {
        Stream.of(channels).forEach(channelConfiguration -> RemoveChannelToPull(channelConfiguration));
        Stream.of(channels).forEach(channelConfiguration -> RemoveChannelToPush(channelConfiguration));
    }

    @Override
    public void RemoveChannelToPull(ChannelConfiguration channel) {
        RemoveChannelFromReplication(_pull, channel.getNameGlobal(), _pullChannels);
        if (channel.getNameLocal() != channel.getNameGlobal()) {
            RemoveChannelFromReplication(_pull, channel.getNameLocal(), _pullChannels);
        }
    }

    @Override
    public void RemoveChannelToPush(ChannelConfiguration channel) {
        RemoveChannelFromReplication(_push, channel.getNameGlobal(), _pushChannels);
        if (channel.getNameLocal() != channel.getNameGlobal()) {
            RemoveChannelFromReplication(_push, channel.getNameLocal(), _pushChannels);
        }
    }

    @Override
    public void RemoveChannelToPushAndPull(ChannelConfiguration channel) {
        RemoveChannelToPull(channel);
        RemoveChannelToPush(channel);
    }

    @Override
    public void StartPull() {
        _pull.start();
    }

    @Override
    public void StartPush() {
        _push.start();
    }

    @Override
    public void StartSync() {
        StartPush();
        StartPull();
    }

    @Override
    public void StopPull() {
        _pull.stop();
    }

    @Override
    public void StopPush() {
        _push.stop();
    }

    @Override
    public void StopSync() {
        StopPull();
        StopPush();
    }

    @Override
    public void restart() throws MalformedURLException {
        _pull.stop();
        _push.stop();
        setupSync();
        _push.start();
        _pull.start();
    }

    @Override
    public String GetPushStatus() {
        return _push.getStatus().toString();
    }

    @Override
    public String GetPullStatus() {
        return _pull.getStatus().toString();
    }

    @Override
    public String GetPullerProgress() {
        String pullStatus = GetPullStatus();
        if (pullStatus.toLowerCase().contains("processed")) {
            int processed = Integer.parseInt(pullStatus.split("/")[0].split(" ")[1]);
            int toProcess = Integer.parseInt(pullStatus.split("/")[1].split(" ")[1]);
            if (toProcess != 0) {
                int progress = (processed * 100) / toProcess;
                if (progress != LastProgress) {
                    LastProgress = progress;
                    LastCheckTime = new Date();
                }
                return "downloading...";
            }
        } else if (pullStatus.toLowerCase() == "idle" || pullStatus.toLowerCase() == "stopped") {
            return "completed";
        }
        return "unknown";
    }

    @Override
    public List<String> GetPullChannels() {
        return _pullChannels;
    }

    @Override
    public List<String> GetPushChannels() {
        return _pushChannels;
    }

    private void AddChannelToReplication(Replication replication, String channel, List<String> channels) {
        if (!channel.isEmpty()) {
            if (!Stream.of(channels).filter(value -> value == channel).findFirst().isPresent()) {
                channels.add(channel);
                replication.setChannels(channels);
                replication.restart();
            }
        }
    }

    private void RemoveChannelFromReplication(Replication replication, String channelToRemove, List<String> channels) {
        List<String> filter = (List<String>) Stream.of(channels).filterNot(value -> value == channelToRemove);
        replication.setChannels(filter);
    }
}
