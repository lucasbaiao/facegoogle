package br.com.babuka.couchbase.repository;

import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.babuka.couchbase.services.configuration.ChannelConfiguration;
import br.com.babuka.domain.entities.Category;
import br.com.babuka.domain.interfaces.repositories.CategoriaDataSource;
import io.reactivex.Observable;

public class CouchbaseCategoriasRepository extends CouchbaseBaseRepository<Category, Category.CategoryFactory> implements CategoriaDataSource {

    public CouchbaseCategoriasRepository(Context context) {
        super(ChannelConfiguration.Categorias, context,Category.CategoryFactory.class);
        _channels.add(ChannelConfiguration.Categorias);
    }

    public CouchbaseCategoriasRepository(ChannelConfiguration entityType, Context context) {
        super(entityType, context, Category.CategoryFactory.class);
    }

    @Override
    protected String ViewName() {
        return "CategoryView";
    }

    @Override
    protected String ViewVersion() {
        return "1";
    }

    @NotNull
    @Override
    public List<Category> getAll() {
        try {
            return GetAllCollection();
        } catch (CouchbaseLiteException e) {
            return Collections.emptyList();
        }
    }

    @NotNull
    @Override
    public List<Category> getAll(@NotNull String parentId) {
        List<Category> categories = new ArrayList<>();
        for (Category category : getAll()) {
            if (category.getGroup().equals(parentId)){
                categories.add(category);
            }
        }
        return categories;
    }
}

