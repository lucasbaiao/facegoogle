﻿using Couchbase.Lite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Console
{
    public class CouchbaseCommunicationService : IDisposable
    {
        private Uri _syncGatewayAddress;
        private Replication _pull;
        private Replication _push;
        private List<string> _pullChannels;
        private List<string> _pushChannels;

        public CouchbaseCommunicationService()
        {
            _pullChannels = new List<string>();
            _pushChannels = new List<string>();
        }

        public void SetupSync()
        {
            _syncGatewayAddress = new Uri("http://localhost:4984/db");
            _pull = CouchbaseDatabaseInfo.Database.CreatePullReplication(_syncGatewayAddress);
            _push = CouchbaseDatabaseInfo.Database.CreatePushReplication(_syncGatewayAddress);
            _pull.Channels = _pullChannels;
            _push.Channels = _pushChannels;
            _push.Continuous = true;
            _pull.Continuous = true;
        }

        private void _push_Changed(object sender, ReplicationChangeEventArgs e)
        {
            if (e.Status == ReplicationStatus.Stopped)
            {
                _push.Start();
            }
            if (e.LastError != null)
            {
                System.Console.Write(e.LastError.Message);
                System.Console.Write(e.LastError.StackTrace);
            }
        }

        private void _pull_Changed(object sender, ReplicationChangeEventArgs e)
        {
            if (e.Status == ReplicationStatus.Stopped)
            {
                _pull.Start();
            }
            if (e.LastError != null)
            {
                System.Console.Write(e.LastError.Message);
                System.Console.Write(e.LastError.StackTrace);
            }
        }

        public void StopPush()
        {
            _push.Stop();
        }

        public void StartPush()
        {
            _push.Start();
        }

        public void StopPull()
        {
            _pull.Stop();
        }

        public void StartPull()
        {
            _pull.Start();
        }

        public void StartSync()
        {
            StartPush();
            StartPull();
        }

        public void AddChannelsToPull(List<ChannelConfiguration> channels)
        {
            channels.ForEach(c => AddChannelToPull(c));
        }

        public void AddChannelsToPush(List<ChannelConfiguration> channels)
        {
            channels.ForEach(c => AddChannelToPush(c));
        }

        public void AddChannelsToPushAndPull(List<ChannelConfiguration> channels)
        {
            AddChannelsToPull(channels);
            AddChannelsToPush(channels);
        }

        public void RemoveChannelsToPushAndPull(List<ChannelConfiguration> channels)
        {
            channels.ForEach(c => RemoveChannelToPull(c));
            channels.ForEach(c => RemoveChannelToPush(c));
        }


        public void AddChannelToPush(ChannelConfiguration channel)
        {
            AddChannelToReplication(_push, channel.NameLocal, _pushChannels);
        }

        public void AddChannelToPull(ChannelConfiguration channel)
        {
            AddChannelToReplication(_pull, channel.NameLocal, _pullChannels);
            if (channel.NameLocal != channel.NameGlobal)
            {
                AddChannelToReplication(_pull, channel.NameGlobal, _pullChannels);
            }
        }

        private void AddChannelToReplication(Replication replication, string channel, List<string> channels)
        {

            if (!string.IsNullOrEmpty(channel))
            {
                if (channels.FirstOrDefault(p => p == channel) == null)
                {
                    channels.Add(channel);
                    replication.Channels = channels;
                    replication.Restart();
                }
            }
        }

        public void RemoveChannelToPull(ChannelConfiguration channel)
        {
            RemoveChannelFromReplication(_pull, channel.NameGlobal, _pullChannels);
            if (channel.NameLocal != channel.NameGlobal)
            {
                RemoveChannelFromReplication(_pull, channel.NameLocal, _pullChannels);
            }
        }

        public void RemoveChannelToPush(ChannelConfiguration channel)
        {
            RemoveChannelFromReplication(_push, channel.NameGlobal, _pushChannels);
            if (channel.NameLocal != channel.NameGlobal)
            {
                RemoveChannelFromReplication(_push, channel.NameLocal, _pushChannels);
            }
        }

        private void RemoveChannelFromReplication(Replication replication, string channelToRemove, List<string> channels)
        {
            channels.RemoveAll(p => p == channelToRemove);
            replication.Channels = channels;
        }

        public void AddChannelToPushAndPull(ChannelConfiguration channel)
        {
            AddChannelToPull(channel);
            AddChannelToPush(channel);
        }

        public void RemoveChannelToPushAndPull(ChannelConfiguration channel)
        {
            RemoveChannelToPull(channel);
            RemoveChannelToPush(channel);
        }

        public void StopSync()
        {
            StopPull();
            StopPush();
        }

        public void SetupCommunication()
        {
            SetupSync();
            Restart();
        }
        
        public void Restart()
        {
            _pull.Stop();
            _push.Stop();
            SetupSync();
            _push.Start();
            _pull.Start();
        }

        public void Dispose()
        {
            CouchbaseDatabaseInfo.Database.Dispose();
            CouchbaseDatabaseInfo.Manager.Close();
        }

        public string GetPushStatus()
        {
            if (_push.ActiveTaskInfo.ContainsKey("status"))
            {
                return _push.ActiveTaskInfo["status"].ToString();
            }
            return _push.Status.ToString();
        }

        public string GetPullStatus()
        {
            if (_pull.ActiveTaskInfo.ContainsKey("status"))
            {
                return _pull.ActiveTaskInfo["status"].ToString();
            }
            return _pull.Status.ToString();
        }
        
        public double LastProgress { get; private set; }
        public DateTime LastCheckTime { get; private set; }

        public string GetPullerProgress()
        {
            var pullStatus = GetPullStatus();
            if (pullStatus.ToLower().Contains("processed"))
            {
                var processed = Convert.ToInt32(pullStatus.Split('/')[0].Split(' ')[1]);
                var toProcess = Convert.ToInt32(pullStatus.Split('/')[1].Split(' ')[1]);
                if (toProcess != 0)
                {
                    var progress = (processed * 100) / toProcess;
                    if (progress != LastProgress)
                    {
                        LastProgress = progress;
                        LastCheckTime = DateTime.Now;
                    }
                    else if ((DateTime.Now - LastCheckTime).TotalSeconds > 15)
                    {
                        return "completed";

                    }
                    return "downloading...";
                }
            }
            else if (pullStatus.ToLower() == "idle" || pullStatus.ToLower() == "stopped")
            {
                return "completed";
            }
            return "unknown";
        }

        public IList<string> GetPullChannels()
        {
            return _pullChannels;
        }

        public IList<string> GetPushChannels()
        {
            return _pushChannels;
        }
    }
}
