﻿using Console;
using Newtonsoft.Json;
using RTM.Global.ValueObjects.RTM.Administrativo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RTM.Global.Entities.Empresa
{
    public class Empresa : IEntity<Empresa>
    {
        public static EmpresaFactory Factory = new EmpresaFactory();
        public string DocumentId { get; private set; }
        public string RazaoSocial { get; private set; }
        public string NomeFantasia { get; private set; }
        public int Codigo { get; private set; }
        public string CNPJ { get; private set; }
        public string NomeResponsavel { get; private set; }
        public string Email { get; private set; }
        public Endereço Endereco { get; private set; }

        public IFactory<Empresa> FactoryInstance { get; private set; }

        private Empresa(string razaoSocial, string nomeFantasia, int codigo, string cnpj, string nomeResponsavel,
            Endereço endereco, string email)
        {
            FactoryInstance = new EmpresaFactory(this);
            RazaoSocial = razaoSocial;
            NomeFantasia = nomeFantasia;
            Codigo= codigo;
            CNPJ = cnpj;
            NomeResponsavel = nomeResponsavel;
            Endereco = endereco;
            Email = email;
        }

        public class EmpresaFactory : IEntityFactory<Empresa, EmpresaFactory>
        {
            public Empresa Instance { get; private set; }
            public EmpresaFactory() { }

            public EmpresaFactory(Empresa empresa) { Instance = empresa; }

            [JsonConstructor]
            public EmpresaFactory(string razaoSocial, string nomeFantasia, int codigoFranquia, string cnpj, string nomeResponsavel, Endereço.EnderecoFactory endereco, string email)
            {
                Instance = new Empresa(razaoSocial, nomeFantasia, codigoFranquia, cnpj, nomeResponsavel, endereco?.Instance, email);
            }

            public EmpresaFactory Create(string razaoSocial, string nomeFantasia, int codigoFranquia, string cnpj, string nomeResponsavel,
                Endereço endereco, string email)
            {
                Instance = new Empresa(razaoSocial, nomeFantasia, codigoFranquia, cnpj, nomeResponsavel, endereco, email);
                return this;
            }

            public EmpresaFactory From(Empresa empresa)
            {
                Instance = empresa;
                return this;
            }
       
            public EmpresaFactory WithId(string documentId)
            {
                Instance.DocumentId = documentId;
                return this;
            }

            internal EmpresaFactory WithNome(string nome)
            {
                Instance.RazaoSocial = nome;
                return this;
            }

            internal EmpresaFactory WithEndereco(Endereço endereco)
            {
                Instance.Endereco = endereco;
                return this;
            }
        }
    }
}