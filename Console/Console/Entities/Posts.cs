﻿using Console.Interfaces;
using Console.ValueObjects;
using Newtonsoft.Json;
using System;
using System.IO;

namespace Console
{
    public class Posts : IEntity<Posts>
    {
        public static PostsFactory Factory = new PostsFactory();
        public string DocumentId { get; private set; }
        public string EmpresaId { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }
        public string[] Keywords { get; private set; }

        [JsonIgnore]
        public Stream Imagem { get; private set; }

        [JsonConverter(typeof(RichDudeConverter))]
        public ITipoPost Tipo { get; private set; }

        public IFactory<Posts> FactoryInstance { get; private set; }


        private Posts(string empresaId, string title, string description, string[] keywords, ITipoPost tipo)
        {
            EmpresaId = empresaId;
            Description = description;
            Title = title;
            Tipo = tipo;
            Keywords = keywords;
        }

        public class PostsFactory : IEntityFactory<Posts, PostsFactory>
        {
            public Posts Instance { get; private set; }

            internal PostsFactory()
            { }

            [JsonConstructor]
            public PostsFactory(string empresaId, string title, string description, string[] keywords, [JsonConverter(typeof(RichDudeConverter))] ITipoPost tipo)
            {
                Instance = new Posts(empresaId, title, description, keywords, tipo);
            }

            public PostsFactory From(Posts obj)
            {
                Instance = (Posts)obj.MemberwiseClone();
                return this;
            }

            public PostsFactory WithId(string id)
            {
                Instance.DocumentId = id;
                return this;
            }

            internal PostsFactory Create(string empresaId, string title, string description, string[] keys, ITipoPost tipo)
            {
                Instance = new Posts(empresaId, title, description, keys, tipo);
                return this;
            }
            
            internal PostsFactory WithEmpresa(string empresaId)
            {
                Instance.EmpresaId = empresaId;
                return this;
            }

            public PostsFactory WithImage(Stream imagem)
            {
                Instance.Imagem = imagem;
                return this;
            }

            internal PostsFactory WithName(string text)
            {
                Instance.Title = text;
                return this;
            }
        }


        public class RichDudeConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return (objectType == typeof(ITipoPost));
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                object richDude = serializer.Deserialize<PostTipoCardapio>(reader);
                if (richDude == null)
                {
                    richDude = serializer.Deserialize<PostTipoCardapio>(reader);
                }
                
                return richDude;
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                // Left as an exercise to the reader :)
                serializer.Serialize(writer, value);
            }
        }
    }
}