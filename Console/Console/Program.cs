﻿using Console.ValueObjects;
using RTM.Global.Entities.Administrativo;
using RTM.Global.Entities.Empresa;
using RTM.Global.ValueObjects.RTM.Administrativo;
using RTM.Global.ValueObjects.RTM.Administrativo.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Console
{
    class Program
    {
        PostsRepository postsRepository = new PostsRepository();
        EmpresaRepository empresaRepository = new EmpresaRepository();

        private void AtualizarEmpresaPosts(string id)
        {
            var empresa = empresaRepository.GetById(id);
            if (empresa != null)
            {
                postsRepository.GetAll().ForEach(p =>
                {
                    var toUpdate = Posts.Factory.From(p).WithEmpresa(empresa.DocumentId).Instance;
                    postsRepository.Update(toUpdate);
                });
            }
            else
            {
                System.Console.WriteLine("Empresa não encontrada");
            }
        }

        static void Main(string[] args)
        {
            var program = new Program();
            
            System.Console.WriteLine("Hello ... Manager setup, wait!");
            program.ConfigureManager();
            System.Console.WriteLine("Setup finish!");

            while (true)
            {
                var key = program.ReadOption();
                {
                    if (key == System.ConsoleKey.U)
                    {
                        System.Console.WriteLine("Digite o id da empresa");
                        var id = System.Console.ReadLine();
                        program.AtualizarEmpresaPosts(id);
                    }
                    else if (key == System.ConsoleKey.D1 || key == System.ConsoleKey.NumPad1)
                    {
                        System.Console.WriteLine("Lendo posts");
                        program.postsRepository.GetAll().ForEach(p =>
                        {
                            System.Console.WriteLine(string.Format("id-> {0}, {1}", p.DocumentId, p.Title));
                        });

                        System.Console.WriteLine("Lendo empresas");
                        program.empresaRepository.GetAll().ForEach(p =>
                        {
                            System.Console.WriteLine(string.Format("id-> {0}, {1}", p.DocumentId, p.CNPJ));
                        });


                        System.Console.WriteLine("Finish");
                        System.Console.ReadKey();
                    }
                    else if (key == System.ConsoleKey.D2 || key == System.ConsoleKey.NumPad2)
                    {
                        program.CriarPost();
                    }
                    else if (key == System.ConsoleKey.D3 || key == System.ConsoleKey.NumPad3)
                    {
                        program.AtualizarPost();
                    }
                    else if (key == System.ConsoleKey.D4 || key == System.ConsoleKey.NumPad4)
                    {
                        program.CriarEmpresa();
                    }
                    else if (key == System.ConsoleKey.D5 || key == System.ConsoleKey.NumPad5)
                    {
                        program.InserirImagem();
                    }
                }                
            };
        }

        private void InserirImagem()
        {
            System.Console.WriteLine("Digite o id do post");
            var postId = System.Console.ReadLine();
            System.Console.WriteLine("Caminho da imagem");
            var path = System.Console.ReadLine();


            var old = postsRepository.GetById(postId);
            if (old != null)
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                postsRepository.UpsertImagem(old.DocumentId, fs);
            }
            else
            {
                System.Console.WriteLine("Post nao encontrado");
            }
        }

        private Posts GetPost()
        {
            System.Console.WriteLine("Digite o id da empresa");
            var empresaId = System.Console.ReadLine();
            System.Console.WriteLine("Digite um nome");
            var text = System.Console.ReadLine();
            System.Console.WriteLine("Descrição do post");
            var subtitle = System.Console.ReadLine();
            System.Console.WriteLine("Palavras chaves '(separadas por virgula)'");
            var keys = System.Console.ReadLine();

            var tipo = PostTipoPromocao.Factory.Create(Convert.ToDecimal(9.99), 40).Instance;
            return Posts.Factory.Create(empresaId, text, subtitle, keys.Split(','), tipo).Instance;
        }

        private void CriarPost()
        {
            postsRepository.Insert(GetPost());
        }

        private void AtualizarPost()
        {
            System.Console.WriteLine("Digite id do post");
            var id = System.Console.ReadLine();
            var post = GetPost();
            var old = postsRepository.GetById(id);
            if (old != null)
            {
                var toUpdate = Posts.Factory.From(post).WithId(old.DocumentId).Instance;
                postsRepository.Update(toUpdate);
            }
            else
            {
                System.Console.WriteLine("Post nao encontrado");
            }
        }

        private void CriarEmpresa()
        {
            System.Console.WriteLine("Digite um nome");
            var nome = System.Console.ReadLine();
            System.Console.WriteLine("Digite um cnpj");
            var cnpj = System.Console.ReadLine();

            var list = empresaRepository.GetAll() ?? new List<Empresa>();
            var codigo = 0;
            if (list.Count > 0)
                codigo = list.Max(e => e.Codigo);

            var empresa = empresaRepository.GetAll()?.FirstOrDefault(e => e.CNPJ == cnpj);
            if (empresa == null)
            {
                var estado = Estado.Factory.Create(EstadoEnum.PARANA).Instance;
                var munic = Municipio.Factory.Create(estado, 125, "SAP").Instance;
                var logradouro = Logradouro.Factory.Create("Projetada C", 0, "Green Vale", "86455000");
                var endereco = Endereço.Factory.Create(munic, logradouro, string.Empty);
                empresaRepository.Insert(Empresa.Factory.Create(nome, nome, codigo++, cnpj, string.Empty, endereco, string.Empty).Instance);
            }
            else
            {
                var estado = Estado.Factory.Create(EstadoEnum.PARANA).Instance;
                var munic = Municipio.Factory.Create(estado, 125, "SAP").Instance;
                var logradouro = Logradouro.Factory.Create("Projetada C", 0, "Green Vale", "86455000");
                var endereco = Endereço.Factory.Create(munic, logradouro, string.Empty);
                
                empresaRepository.Update(Empresa.Factory.From(empresa).WithEndereco(endereco).WithNome(nome).Instance);
            }
        }


        private bool IsKeyValid(System.ConsoleKey key)
        {
            return key == System.ConsoleKey.D1 || key == System.ConsoleKey.D2 || key == System.ConsoleKey.D3 || key == System.ConsoleKey.D4 || key == System.ConsoleKey.D5
                || key == System.ConsoleKey.U
                || key == System.ConsoleKey.NumPad1 || key == System.ConsoleKey.NumPad2 || key == System.ConsoleKey.NumPad3 || key == System.ConsoleKey.NumPad4 || key == System.ConsoleKey.NumPad5;
        }

        private System.ConsoleKey ReadOption()
        {
            System.ConsoleKey key;
            do
            {
                System.Console.Clear();
                System.Console.WriteLine("****************************************************");
                System.Console.WriteLine("1 - Ler Valores");
                System.Console.WriteLine("2 - Escrever Post");
                System.Console.WriteLine("3 - Atualizar Post");
                System.Console.WriteLine("4 - Criar empresa");
                System.Console.WriteLine("5 - Inserir imagem post");

                System.Console.WriteLine("U - Atualizar empresa dos posts");
                System.Console.WriteLine("****************************************************");
                key = System.Console.ReadKey().Key;
            } while (!IsKeyValid(key));
            return key;
        }

        private void ConfigureManager()
        {
            CouchbaseCommunicationService ccs = new CouchbaseCommunicationService();
            ccs.SetupCommunication();
            ccs.AddChannelToPushAndPull(ChannelConfiguration.Posts);
            ccs.AddChannelToPushAndPull(ChannelConfiguration.Empresa);
        }
    }
}
