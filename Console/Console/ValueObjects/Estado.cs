﻿using Console;
using Newtonsoft.Json;
using RTM.Global.ValueObjects.RTM.Administrativo.Enum;
using System.IO;

namespace RTM.Global.Entities.Administrativo
{
    public class Estado 
    {
        public static readonly EstadoFactory Factory = new EstadoFactory();

        public int Codigo { get; private set; }
        public string UF { get; private set; }
        public string Nome { get; private set; }

        [JsonIgnore]
        public Stream CepFile { get; private set; }

        public IFactory<Estado> FactoryInstance { get; private set; }

        private Estado() => FactoryInstance = new EstadoFactory(null);

        public class EstadoFactory : IFactory<Estado>
        {
            public Estado Instance { get; private set; }

            public EstadoFactory() { }
            public EstadoFactory(Estado estado) { Instance = estado; }

            [JsonConstructor]
            public EstadoFactory(int Codigo, string UF, string Nome)
            {
                Create(Codigo, UF, Nome);
            }

            public EstadoFactory Create(int codigo, string uf, string nome)
            {
                Instance = new Estado
                {
                    Codigo = codigo,
                    UF = uf,
                    Nome = nome,
                    CepFile = null
                };

                return this;
            }

            public EstadoFactory Create(EstadoEnum estadoEnum)
            {
                return Create(estadoEnum.Codigo, estadoEnum.Valor, estadoEnum.Nome);
            }

            public EstadoFactory WithId(string id)
            {
                return this;
            }

            public EstadoFactory From(Estado obj)
            {
                Instance = (Estado)obj.MemberwiseClone();
                return this;
            }
        }
    }

}
