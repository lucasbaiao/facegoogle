﻿using Console;
using Newtonsoft.Json;
using RTM.Global.Entities.Administrativo;
using System;

namespace RTM.Global.ValueObjects.RTM.Administrativo
{
    public class Endereço
    {
        public static readonly EnderecoFactory Factory = new EnderecoFactory();

        public Logradouro Logradouro { get; private set; }
        public Municipio Municipio { get; private set; }
        public string Complemento { get; private set; }

        private Endereço(Municipio municipio, Logradouro logradouro, string complemento)
        {
            Municipio = municipio;
            Logradouro = logradouro;
            Complemento = complemento;
        }

        public class EnderecoFactory : IFactory<Endereço>
        {
            public Endereço Instance { get; private set; }

            public EnderecoFactory() { }

            [JsonConstructor]
            public EnderecoFactory(Municipio.MunicipioFactory municipio, Logradouro.LogradouroFactory logradouro, string complemento)
            {
                Create(municipio.Instance, logradouro.Instance, complemento);
            }

            public Endereço Create(Municipio municipio, Logradouro logradouro, string complemento)
            {
                Instance = new Endereço(municipio, logradouro, complemento);
                return Instance;
            }
        }
    }
}
