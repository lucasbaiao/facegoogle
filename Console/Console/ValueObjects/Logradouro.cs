﻿using Console;
using Newtonsoft.Json;

namespace RTM.Global.ValueObjects.RTM.Administrativo
{
    public class Logradouro
    {
        public static readonly LogradouroFactory Factory = new LogradouroFactory();

        public string Rua { get; private set; }
        public int Numero { get; private set; }
        public string Bairro { get; private set; }
        public string CEP { get; private set; }

        private Logradouro() { }

        private Logradouro(string rua, int numero, string bairro, string cep)
        {
            Rua = rua;
            Numero = numero;
            Bairro = bairro;
            CEP = cep;
        }

        public string GetLogradouro()
        {
            string numero = Numero != default(int) ? Numero.ToString() : "S/N";
            return $"{Rua}, {numero}";
        }

        public class LogradouroFactory : IFactory<Logradouro>
        {
            public Logradouro Instance { get; private set; }

            public LogradouroFactory() { }

            [JsonConstructor]
            public LogradouroFactory(string rua, int numero, string bairro, string cep)
            {
                Create(rua, numero, bairro, cep);
            }

            public Logradouro Create(string rua, int numero, string bairro, string cep)
            {
                Instance = new Logradouro(rua, numero, bairro, cep);
                return Instance;
            }

            public LogradouroFactory WithId(string id)
            {
                throw new System.NotImplementedException();
            }                      
        }
    }
}
