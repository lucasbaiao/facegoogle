﻿using RTM.Global.ValueObjects.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console.ValueObjects
{
    public class TipoPostEnum : BaseEnum<TipoPostEnum, string>
    {
        public static TipoPostEnum Cardapio = new TipoPostEnum(1, "Cardapio");
        public static TipoPostEnum Promocao = new TipoPostEnum(2, "Promocao");

        protected TipoPostEnum(int codigo, string valor) : base(codigo, valor)
        {
        }
    }
}
