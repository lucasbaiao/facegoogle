﻿using Console.Interfaces;
using Newtonsoft.Json;

namespace Console.ValueObjects
{
    public class PostTipoPromocao : ITipoPost
    {
        public static readonly PostTipoPromocaoFactory Factory = new PostTipoPromocaoFactory();

        private PostTipoPromocao(decimal preco, int percentOff)
        {
            Preco = preco;
            PercentOff = percentOff;
        }

        public decimal Preco { get; private set; }
        public int PercentOff { get; private set; }
        public int Codigo { get; private set; }

        public class PostTipoPromocaoFactory : IFactory<PostTipoPromocao>
        {
            public PostTipoPromocao Instance { get; private set; }

            public PostTipoPromocaoFactory() { }

            [JsonConstructor]
            public PostTipoPromocaoFactory(decimal preco, int percentOff)
            {
                Create(preco, percentOff);
            }

            public PostTipoPromocaoFactory Create(decimal preco, int percentOff)
            {
                Instance = new PostTipoPromocao(preco, percentOff);
                return this;
            }
        }
    }
}
