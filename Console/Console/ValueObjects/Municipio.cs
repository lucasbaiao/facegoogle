﻿using Console;
using Newtonsoft.Json;
using System;
using static RTM.Global.Entities.Administrativo.Estado;

namespace RTM.Global.Entities.Administrativo
{
    public class Municipio
    {
        public static readonly MunicipioFactory Factory = new MunicipioFactory();
        
        public IFactory<Municipio> FactoryInstance { get; private set; }

        public int Codigo { get; private set; }
        public string Nome { get; private set; }
        public Estado Estado { get; private set; }
        private Municipio() => FactoryInstance = new MunicipioFactory(null);

        public class MunicipioFactory : IFactory<Municipio>
        {
            public Municipio Instance { get; private set; }

            public MunicipioFactory() { }
            public MunicipioFactory(Municipio municipio) { Instance = municipio; }

            [JsonConstructor]
            public MunicipioFactory(EstadoFactory Estado, double Codigo, string Nome)
            {
                Create(Estado?.Instance, Convert.ToInt32(Codigo), Nome);
            }

            public MunicipioFactory Create(Estado estado, int codigo, string nome)
            {
                Instance = new Municipio
                {
                    Estado = estado,
                    Codigo = codigo,
                    Nome = nome
                };

                return this;
            }

            public MunicipioFactory WithId(string id)
            {
                return this;
            }

            public MunicipioFactory WithEstado(Estado estado)
            {
                Instance.Estado = estado;
                return this;
            }

            public MunicipioFactory From(Municipio obj)
            {
                Instance = (Municipio)obj.MemberwiseClone();
                return this;
            }
        }

    }
}
