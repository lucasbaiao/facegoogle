﻿using RTM.Global.Entities.Administrativo;
using RTM.Global.ValueObjects.Enum;
using System.Linq;

namespace RTM.Global.ValueObjects.RTM.Administrativo.Enum
{
    public class EstadoEnum : BaseEnum<EstadoEnum, string>
    {
        public string Nome { get; private set; }

        private const string UF_RONDONIA = "RO";
        private const string UF_ACRE = "AC";
        private const string UF_AMAZONAS = "AM";
        private const string UF_RORAIMA = "RR";
        private const string UF_PARA = "PA";
        private const string UF_AMAPA = "AP";
        private const string UF_TOCANTINS = "TO";
        private const string UF_MARANHAO = "MA";
        private const string UF_PIAUI = "PI";
        private const string UF_CEARA = "CE";
        private const string UF_RIO_GRANDE_NORTE = "RN";
        private const string UF_PARAIBA = "PB";
        private const string UF_PERNAMBUCO = "PE";
        private const string UF_ALAGOAS = "AL";
        private const string UF_SERGIPE = "SE";
        private const string UF_BAHIA = "BA";
        private const string UF_MINAS_GERAIS = "MG";
        private const string UF_ESPIRITO_SANTO = "ES";
        private const string UF_RIO_JANEIRO = "RJ";
        private const string UF_SAO_PAULO = "SP";
        private const string UF_PARANA = "PR";
        private const string UF_SANTA_CATARINA = "SC";
        private const string UF_RIO_GRANDE_SUL = "RS";
        private const string UF_MATO_GROSSO_SUL = "MS";
        private const string UF_MATO_GROSSO = "MT";
        private const string UF_GOIAS = "GO";
        private const string UF_DISTRITO_FEDERAL = "DF";

        public static readonly EstadoEnum RONDONIA = new EstadoEnum(11, UF_RONDONIA, "Rondônia");
        public static readonly EstadoEnum ACRE = new EstadoEnum(12, UF_ACRE, "Acre");
        public static readonly EstadoEnum AMAZONAS = new EstadoEnum(13, UF_AMAZONAS, "Amazonas");
        public static readonly EstadoEnum RORAIMA = new EstadoEnum(14, UF_RORAIMA, "Roraima");
        public static readonly EstadoEnum PARA = new EstadoEnum(15, UF_PARA, "Pará");
        public static readonly EstadoEnum AMAPA = new EstadoEnum(16, UF_AMAPA, "Amapá");
        public static readonly EstadoEnum TOCANTINS = new EstadoEnum(17, UF_TOCANTINS, "Tocantins");
        public static readonly EstadoEnum MARANHAO = new EstadoEnum(21, UF_MARANHAO, "Maranhão");
        public static readonly EstadoEnum PIAUI = new EstadoEnum(22, UF_PIAUI, "Piauí");
        public static readonly EstadoEnum CEARA = new EstadoEnum(23, UF_CEARA, "Ceará");
        public static readonly EstadoEnum RIO_GRANDE_NORTE = new EstadoEnum(24, UF_RIO_GRANDE_NORTE, "Rio Grande do Norte");
        public static readonly EstadoEnum PARAIBA = new EstadoEnum(25, UF_PARAIBA, "Paraíba");
        public static readonly EstadoEnum PERNAMBUCO = new EstadoEnum(26, UF_PERNAMBUCO, "Pernambuco");
        public static readonly EstadoEnum ALAGOAS = new EstadoEnum(27, UF_ALAGOAS, "Alagoas");
        public static readonly EstadoEnum SERGIPE = new EstadoEnum(28, UF_SERGIPE, "Sergipe");
        public static readonly EstadoEnum BAHIA = new EstadoEnum(29, UF_BAHIA, "Bahia");
        public static readonly EstadoEnum MINAS_GERAIS = new EstadoEnum(31, UF_MINAS_GERAIS, "Minas Gerais");
        public static readonly EstadoEnum ESPIRITO_SANTO = new EstadoEnum(32, UF_ESPIRITO_SANTO, "Espírito Santo");
        public static readonly EstadoEnum RIO_JANEIRO = new EstadoEnum(33, UF_RIO_JANEIRO, "Rio de Janeiro");
        public static readonly EstadoEnum SAO_PAULO = new EstadoEnum(35, UF_SAO_PAULO, "São Paulo");
        public static readonly EstadoEnum PARANA = new EstadoEnum(41, UF_PARANA, "Paraná");
        public static readonly EstadoEnum SANTA_CATARINA = new EstadoEnum(42, UF_SANTA_CATARINA, "Santa Catarina");
        public static readonly EstadoEnum RIO_GRANDE_SUL = new EstadoEnum(43, UF_RIO_GRANDE_SUL, "Rio Grande do Sul");
        public static readonly EstadoEnum MATO_GROSSO_SUL = new EstadoEnum(50, UF_MATO_GROSSO_SUL, "Mato Grosso do Sul");
        public static readonly EstadoEnum MATO_GROSSO = new EstadoEnum(51, UF_MATO_GROSSO, "Mato Grosso");
        public static readonly EstadoEnum GOIAS = new EstadoEnum(52, UF_GOIAS, "Goiás");
        public static readonly EstadoEnum DISTRITO_FEDERAL = new EstadoEnum(53, UF_DISTRITO_FEDERAL, "Distrito Federal");

        protected EstadoEnum(int codigo, string uf, string nome) : base(codigo, uf)
        {
            Nome = nome;
        }

        public static Estado GetEstadoByUF(string uf)
        {
            EstadoEnum estado = ToList().FirstOrDefault(p => p.Valor == uf);
            return estado == null ? null : Estado.Factory.Create(estado).Instance;
        }

        public static Estado ToEstado(string uf) => Estado.Factory.Create((EstadoEnum)uf).Instance;
    }
}
