﻿namespace Console.Interfaces
{
    public interface IBaseEnum<T> 
    {

        int Codigo { get; }
        T Valor { get; }

    }
}
