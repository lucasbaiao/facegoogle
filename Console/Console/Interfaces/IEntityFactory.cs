﻿namespace Console
{
    public interface IEntityFactory<T, K> : IFactory<T>
        where T : IEntity<T>
        where K : IEntityFactory<T, K>
    {
        K From(T obj);
        K WithId(string id);
    }
}