﻿using Newtonsoft.Json;

namespace Console
{
    public interface IEntity<T> where T : IEntity<T>
    {
        [JsonIgnore]
        IFactory<T> FactoryInstance { get; }
        string DocumentId { get; }
    }
}