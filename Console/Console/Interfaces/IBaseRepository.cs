﻿using System.Collections.Generic;

namespace Console
{
    public interface IBaseRepository<T, IEntityFactory> where T : IEntity<T>
    {
        void Delete(string documentId);
        string Insert(T entity);
        void Update(T entity);
        T GetById(string entity);
        string Insert(T entity, params ChannelConfiguration[] channels);
        void InsertChannel(ChannelConfiguration channel, T documentId);
        //string CreateAttachment(string entityName);
        ChannelConfiguration[] GetChannels(string documentId);
        T GetByIdAndType(string entity);
        IReadOnlyList<KeyValuePair<string, T>> GetConflicts(string documentId);
        void SolveConflicts(string documentId, string winnerRevisionId);
    }
}