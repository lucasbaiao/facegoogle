﻿namespace Console
{
    public interface IFactory<T>
    {
        T Instance { get; }
    }
}