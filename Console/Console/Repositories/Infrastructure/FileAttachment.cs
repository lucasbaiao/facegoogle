﻿using System.IO;

namespace Console.Infrastructure
{
    public class FileAttachment
    {
        private static string CONTENT_TYPE_JPG = "image/jpg";

        public static CouchbaseAttachmentFactory Factory { get; private set; }
        public string Name { get; private set; }
        public Stream Content { get; private set; }
        public string ContentType { get; private set; }

        static FileAttachment()
        {
            Factory = new CouchbaseAttachmentFactory();
        }

        private FileAttachment(string name, Stream content, string contentType)
        {
            Name = name;
            Content = content;
            ContentType = contentType;
        }

        public class CouchbaseAttachmentFactory
        {
            public FileAttachment Create(string name, Stream content, string contentType)
            {
                return new FileAttachment(name, content, contentType);
            }

            public FileAttachment CreateJpg(string name, Stream content)
            {
                return new FileAttachment(name, content, CONTENT_TYPE_JPG);
            }
        }
    }
}
