﻿using RTM.Global.Entities.Empresa;
using System;
using System.Collections.Generic;

namespace Console
{
    public class EmpresaRepository : CouchbaseBaseRepository<Empresa, Empresa.EmpresaFactory>
    {
        public EmpresaRepository() : base(ChannelConfiguration.Empresa)
        {
            _channels.Add(ChannelConfiguration.Empresa);
        }

        public EmpresaRepository(ChannelConfiguration entityType) : base(entityType)
        {
        }

        protected override string ViewName => "EmpresaView";
        protected override string ViewVersion => "1";

        internal List<Empresa> GetAll()
        {
            return base.GetAllCollection();
        }
    }
}
