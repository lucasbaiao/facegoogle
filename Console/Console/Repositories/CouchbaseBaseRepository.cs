﻿using Couchbase.Lite;
using Newtonsoft.Json;
using System;
using MoreLinq;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Console.Infrastructure;

namespace Console
{
    public abstract class CouchbaseBaseRepository<T, K> : IBaseRepository<T, K>
         where T : IEntity<T>
         where K : IEntityFactory<T, K>
    {
        private readonly bool useCache;
        private List<T> _allCollection { get; set; }
        protected abstract string ViewName { get; }
        protected abstract string ViewVersion { get; }
        protected Database Database { get; private set; }
        protected List<ChannelConfiguration> _channels;
        protected string _entityType;

        public CouchbaseBaseRepository(ChannelConfiguration entityType)
        {
            _allCollection = new List<T>();
            _entityType = entityType.NameGlobal;
            _channels = new List<ChannelConfiguration>();
            Database = CouchbaseDatabaseInfo.Database;
        }

       protected List<T> GetAllCollection()
        {
            
                var result = new List<T>();
                GetQueryAllDocumentsNoCache().CreateQuery().Run().ToList().ForEach(row =>
                {
                    try
                    {
                        var doc = Database.GetDocument(row.DocumentId);
                        if (doc != null && !doc.Deleted)
                        {
                            result.Add(JsonConvert.DeserializeObject<K>(row.Value.ToString()).WithId(row.DocumentId).Instance);

                        }
                    }
                    catch (Exception e)
                    {
                    }
                });

                return result;
            
        }

        protected View GetQueryAllDocumentsNoCache()
        {
            var view = Database.GetView(ViewName);
            view.UpdateIndex();
            if (view.Map == null)
            {
                view.SetMap((doc, emit) =>
                {
                    try
                    {
                        if (IsDocACurrentEntity(doc))
                        {
                            emit(typeof(T).ToString(), GetDocumentValue(doc));
                        }
                    }
                    catch (ArgumentException e)
                    {
                    }

                }, ViewVersion);
            }
            return view;
        }

        protected bool IsDocACurrentEntity(IDictionary<string, object> doc)
        {
            if (doc.Keys.ToList().Exists(p => p == "Type"))
            {
                return doc["Type"].ToString() == _entityType;
            }
            return false;
        }

        protected Document CreateDocument(IEntity<T> obj, List<string> channels)
        {
            var documentTemplate = GetDocumentTemplate(obj, channels);
            var doc = Database.CreateDocument();
            doc.PutProperties(documentTemplate);
            return doc;
        }

        protected void SetAttachment(Document doc, FileAttachment attachment)
        {
            var newRevision = doc.CurrentRevision.CreateRevision();
            newRevision.SetAttachment(attachment.Name, attachment.ContentType, attachment.Content);
            newRevision.Save();
        }

        private IDictionary<string, object> GetDocumentTemplate(IEntity<T> obj, List<string> channels)
        {
            return new Dictionary<string, object>
            {
                {"Type" , _entityType },
                {"Content", obj },
                {"channels", channels }
            };
        }

        public virtual string Insert(T entity)
        {
            return Insert(entity, new List<ChannelConfiguration>().ToArray());
        }

        public virtual string Insert(T entity, params ChannelConfiguration[] channels)
        {
            List<string> channelsNames = new List<string>();
            List<ChannelConfiguration> channelsList = _channels.Where(p => p.NameGlobal != string.Empty).Concat(channels).ToList();
            channelsList.Add(ChannelConfiguration.EmpresaChannel);
            channelsList.ForEach(c =>
            {

                if (c.NameGlobal != string.Empty)
                    channelsNames.Add(c.NameGlobal);

                if (c.NameGlobal != c.NameLocal)
                {
                    channelsNames.Add(c.NameLocal);
                }
            });

            return CreateDocument(entity, channelsNames).Id;
        }

        public void Delete(string documentId)
        {
            Database.GetDocument(documentId).Delete();
            if (useCache)
            {
                lock (_allCollection)
                {
                    _allCollection.RemoveAll(p => (p as IEntity<T>).DocumentId == documentId);
                }
            }
        }

        public virtual void Update(T entity)
        {
            try
            {
                var doc = Database.GetDocument(entity.DocumentId);

                doc.Update((UnsavedRevision newRevision) =>
                {
                    var props = newRevision.Properties;
                    props["Content"] = entity;
                    return true;
                });
            }
            catch (Exception error)
            {
                throw new ArgumentException();
            }
        }

        public void InsertChannel(ChannelConfiguration channel, T entity)
        {
            var doc = Database.GetDocument(entity.DocumentId);
            var currentChannels = JsonConvert.DeserializeObject<List<string>>(doc.UserProperties["channels"].ToString());
            if (!currentChannels.Exists(p => p == channel.NameGlobal))
            {
                currentChannels.Add(channel.NameGlobal);
                doc.Update((UnsavedRevision newRevision) =>
                {
                    var props = newRevision.Properties;
                    props["channels"] = currentChannels;
                    return true;
                });
            }
            if (!currentChannels.Exists(p => p == channel.NameLocal))
            {
                currentChannels.Add(channel.NameLocal);
                doc.Update((UnsavedRevision newRevision) =>
                {
                    var props = newRevision.Properties;
                    props["channels"] = currentChannels;
                    return true;
                });
            }
        }

        public ChannelConfiguration[] GetChannels(string documentId)
        {
            var channelsResult = new List<ChannelConfiguration>();

            var doc = Database.GetDocument(documentId);
            if (doc != null)
            {
                var channels = JsonConvert.DeserializeObject<List<string>>(doc.UserProperties["channels"].ToString());
                if (channels != null)
                {
                    channels.ForEach(c => channelsResult.Add(new ChannelConfiguration(c)));
                }
            }

            return channelsResult.ToArray();
        }

        protected string GetDocumentType(IDictionary<string, object> doc)
        {
            if (doc.Keys.ToList().Exists(p => p == "Type"))
            {
                return doc["Type"].ToString();
            }
            else
            {
                throw new ArgumentException("Entidade fora do padrão RTM");
            }
        }

        protected IDictionary<string, object> ConvertObjectToDictionary(object obj)
        {
            var stringJson = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<IDictionary<string, object>>(stringJson);
        }

        protected object GetDocumentValue(IDictionary<string, object> doc)
        {
            return doc["Content"];
        }

        public virtual T GetById(string documentId)
        {
            try
            {
                if (useCache)
                {
                    return _allCollection.FirstOrDefault(p => p.DocumentId == documentId);
                }
                else
                {
                    var doc = Database.GetDocument(documentId);
                    if (doc.Properties["Content"] as IEntity<T> != null)
                    {
                        return (T)doc.Properties["Content"];
                    }
                    else
                    {
                        var data = (GetDocumentValue(doc.UserProperties).ToString());
                        return JsonConvert.DeserializeObject<K>
                            (data).WithId(documentId).Instance;
                    }
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public T GetByIdAndType(string documentId)
        {
            try
            {
                if (useCache)
                {
                    return _allCollection.FirstOrDefault(p => p.DocumentId == documentId);
                }
                else
                {
                    var doc = Database.GetDocument(documentId);
                    if (doc.Properties["Content"] as IEntity<T> != null)
                    {
                        return (T)doc.Properties["Content"];
                    }
                    else
                    {
                        if (GetDocumentType(doc.UserProperties) != _entityType)
                        {
                            return default(T);
                        }
                        var data = (GetDocumentValue(doc.UserProperties).ToString());
                        return JsonConvert.DeserializeObject<K>
                            (data).WithId(documentId).Instance;
                    }
                }
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public IReadOnlyList<KeyValuePair<string, T>> GetConflicts(string documentId)
        {
            var @default = new List<KeyValuePair<string, T>>();
            try
            {
                var doc = Database.GetDocument(documentId);
                var revisions = doc.LeafRevisions.Where(rev => !rev.IsDeletion).ToList();
                if (revisions.Count <= 1)
                {
                    return @default;
                }

                if ((doc.Properties["Content"] as IEntity<T>) != null)
                {
                    return revisions.Select(rev => new KeyValuePair<string, T>(rev.Id, (T)rev.Properties["Content"])).ToList();
                }
                else
                {
                    return doc.LeafRevisions.Select(rev => new KeyValuePair<string, T>(rev.Id, JsonConvert.DeserializeObject<K>(rev.UserProperties["Content"].ToString()).Instance)).ToList();
                }
            }
            catch { return @default; }
        }

        public void SolveConflicts(string documentId, string winnerRevisionId)
        {
            var doc = Database.GetDocument(documentId);
            doc.LeafRevisions.ForEach(rev =>
            {
                if (rev.Id != winnerRevisionId)
                {
                    rev.DeleteDocument();
                }
            });
        }

        protected virtual Stream GetContentStream(Attachment attachment)
        {
            if (!attachment.ContentStream.CanSeek || !attachment.ContentStream.CanRead)
            {
                var buffer = attachment.Content.ToArray();
                var stream = new MemoryStream(buffer.Length);
                stream.Write(buffer, 0, buffer.Length);
                return stream;
            }
            return attachment.ContentStream;
        }
    }
}
