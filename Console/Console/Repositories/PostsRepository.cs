﻿using Console.Infrastructure;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Console
{
    public class PostsRepository : CouchbaseBaseRepository<Posts, Posts.PostsFactory>
    {
        private const string Imagem = "posts.jpg";
        private const string ContentType = "image/jpg";

        public PostsRepository() : base(ChannelConfiguration.Posts)
        {
            _channels.Add(ChannelConfiguration.Posts);
        }

        public PostsRepository(ChannelConfiguration entityType) : base(entityType)
        {
        }

        protected override string ViewName => "PostsView";
        protected override string ViewVersion => "1";

        internal List<Posts> GetAll()
        {
            return base.GetAllCollection();
        }

        public Stream GetImagem(Posts post)
        {
            return GetImagem(post.DocumentId);
        }

        private Stream GetImagem(string DocumentId)
        {
            var doc = Database.GetDocument(DocumentId);
            Stream result = null;
            if (doc?.CurrentRevision?.GetAttachment(Imagem)?.Content.Count() != 0)
            {
                result = doc.CurrentRevision.GetAttachment(Imagem)?.ContentStream;
            }
            return result;
        }

        public Posts UpsertImagem(string documentId, Stream image)
        {
            var produto = Database.GetDocument(documentId);
            SetAttachment(produto, FileAttachment.Factory.Create(Imagem, image, ContentType));
            var prod = GetById(documentId);
            return Posts.Factory.From(prod).WithImage(GetImagem(prod)).Instance;
        }
        public override string Insert(Posts entity)
        {
            return Insert(entity, new List<ChannelConfiguration>().ToArray());
        }

        public override string Insert(Posts entity, params ChannelConfiguration[] channels)
        {
            var id = base.Insert(entity, channels);
            if (entity.Imagem != null)
            {
                var anexo = FileAttachment.Factory.Create(Imagem, entity.Imagem, ContentType);
                SetAttachment(Database.GetDocument(id), anexo);
            }
            return id;
        }
    }
}
