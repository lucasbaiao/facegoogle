﻿namespace Console
{
    public class ChannelConfiguration
    {
        public static ChannelConfiguration EmpresaChannel = new ChannelConfiguration("EmpresaToken");
        public static ChannelConfiguration Posts = new ChannelConfiguration("Posts");
        public static ChannelConfiguration Empresa = new ChannelConfiguration("Empresa");


        private string _name;
        public string NameGlobal { get; private set; }
        public string NameLocal
        {
            get
            {
                return GetNomeLocal();
            }
            private set { }
        }

        private string GetNomeLocal()
        {
            var nome = _name;
            if (_name == "EmpresaToken")
            {
                nome = "EmpresaChannel";
                NameGlobal = nome;
            }
            else
            {
                var empresaChannel = "EmpresaChannel";
                nome = $"{_name}_{empresaChannel}".TrimEnd('_');
            }
            return nome;
        }


        public ChannelConfiguration(string name)
        {
            if (name != "EmpresaToken")
            {
                NameGlobal = name;
            }
            else
            {
                NameGlobal = string.Empty;
            }

            _name = name;
        }

        public override string ToString()
        {
            return NameGlobal;
        }
    }
}