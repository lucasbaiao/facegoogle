﻿using Couchbase.Lite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Console
{
    public class CouchbaseDatabaseInfo
    {
        private static string DATABASENAME = "database";
        public static Database Database { get; private set; }
        public static Manager Manager { get; private set; }

        static CouchbaseDatabaseInfo()
        {
            SetUpDatabase();
        }

        private static string CombineCouchbasePath()
        {
            var couchbaseDir = "C:\\temp\\home\\database";
            return couchbaseDir;
        }

        private static void SetUpDatabase()
        {
            try
            {
                if (Manager == null)
                {
                    if (!Directory.Exists("C:\\temp\\home"))
                    {
                        Directory.CreateDirectory("C:\\temp\\home");
                    }

                    File.AppendAllText("C:\\temp\\home\\db.log", "getting the great manager");
                    Manager = new Manager(new DirectoryInfo(CombineCouchbasePath()), ManagerOptions.Default);
                    Database = Manager.GetDatabase(DATABASENAME);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }
    }
}
